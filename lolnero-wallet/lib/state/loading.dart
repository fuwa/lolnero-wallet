/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'dart:async';

import '../logging.dart';
import '../logic/controller/cert.dart' as cert;

import 'prototype.dart';
import 'checkWallet.dart';

class LoadingState extends AppStateAutomata {
  bool showUI = false;

  LoadingState(appHook) : super(appHook);

  Future<AppStateAutomata> next() async {
    if (await shouldExit()) return exitState();

    log.fine('loading state next');

    log.finer('done await loading');

    final _certExist = await cert.certExist();

    if (!_certExist) {
      showUI = true;
      syncState();
    }

    final _cert = await cert.getCertificate();
    log.fine('pri: ${_cert.priKeyPath}');
    log.fine('cert: ${_cert.certPath}');
    log.fine('hash: ${_cert.fingerprint}');

    return CheckWalletState(
      appHook,
      false,
      _cert,
    );
  }
}
