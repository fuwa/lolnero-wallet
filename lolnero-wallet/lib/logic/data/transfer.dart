/*

Copyright 2020 fuwa


This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'dart:math';
import 'dart:core';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../helper.dart';
import '../../logic/data/rpc/typed/transfer.dart';
import '../../logic/data/format.dart';
import '../../config.dart' as config;

List<Transfer> searchTransferMulti(
    final List<Transfer> txs, final List<String> keys) {
  bool searchTx(final Transfer tx, final String x) => x.isEmpty
      ? false
      : x == 'all' ||
          x == '.' ||
          tx.txid.startsWith(x) ||
          isIn(tx.type) && x == 'in' ||
          isOut(tx.type) && x == 'out' ||
          identical(tx.type, TransferType.block) && x == 'block' ||
          tx.height.toString().startsWith(x) ||
          isIn(tx.type) && tx.address.startsWith(x) ||
          getDay(tx).startsWith(x) ||
          getMonth(tx).startsWith(x) ||
          getNumericMonth(tx).startsWith(x) ||
          getYear(tx).startsWith(x) ||
          getShortYear(tx).startsWith(x);

  bool searchTxMultiTokens(final Transfer tx, final List<String> xs) =>
      xs.isEmpty
          ? false
          : xs.map((x) => searchTx(tx, x)).fold(true, (x, y) => x && y);

  return txs.where((tx) => searchTxMultiTokens(tx, keys)).toList();
}

String formatTx(Transfer tx) {
  return [
    tx.height.toString().padLeft(8),
    addSymbol(showAmount(tx.amount).padLeft(8)),
    tx.txid.substring(0, config.c.hashViewBlockLength).padRight(10),
    // tx['address'].substring(0, 6),
  ].join('    ');
}

String formatHotTx(Transfer tx) {
  final _sign = tx.type == TransferType.poolIn ? '+' : '-';
  return [
    (_sign + ' ' + addSymbol(showAmount(tx.amount)).padLeft(10)),
    tx.txid.substring(0, config.c.hashViewBlockLength).padRight(10),
  ].join('    ');
}

String formatSign(Transfer tx) {
  switch (tx.type) {
    // '⊕';
    case TransferType.txIn:
      return '+';
    // '⊖';
    case TransferType.txOut:
      return '-';
    case TransferType.poolIn:
      return '+';
    case TransferType.pendingOut:
      return '-';
    case TransferType.failed:
      return '✗';
    case TransferType.block:
      return '💎';
    default:
      return '⊘';
  }
}

Icon formatIcon(final Transfer tx, {final double size = 24.0}) {
  // PRIMARY #ff895f
  final _color = Color(0xffff895f);

  // ANALOGOUS 1 [500]
  final _colorIn = Color(0xfff71751);

  // TRIADIC 1
  final _colorOut = Color(0xffd4ff5f);

  // ANALOGOUS 2
  final _colorPoolIn = Color(0xffffda5f);

  // TRIADIC 2
  final _colorPendingOut = Color(0xff5fff8a);

  // COMPLEMENTARY
  final _colorBlock = Color(0xff5fd4ff);

  switch (tx.type) {
    case TransferType.txIn:
      return Icon(
        // Icons.local_atm,
        Icons.account_balance_wallet,
        color: _colorIn,
        size: size,
      );
    case TransferType.txOut:
      return Icon(
        Icons.restaurant,
        color: _colorOut,
        size: size,
      );
    case TransferType.poolIn:
      return Icon(
        Icons.call_received,
        color: _colorPoolIn,
        size: size,
      );
    case TransferType.pendingOut:
      return Icon(
        Icons.call_made,
        color: _colorPendingOut,
        size: size,
      );
    case TransferType.failed:
      return Icon(
        Icons.block,
        color: _color,
        size: size,
      );
    case TransferType.block:
      return Icon(
        Icons.vpn_key,
        color: _colorBlock,
        size: size,
      );
    default:
      return Icon(
        Icons.error,
        color: _color,
        size: size,
      );
  }
}

String formatTitle(Transfer tx) {
  final _amount = addSymbol(showAmount(tx.amount));
  return [
    // _height,
    _amount,
  ].join(' ');
}

bool isIn(TransferType x) {
  switch (x) {
    case TransferType.poolIn:
    case TransferType.txIn:
      return true;
    default:
      return false;
  }
}

bool isOut(TransferType x) {
  switch (x) {
    case TransferType.pendingOut:
    case TransferType.txOut:
      return true;
    default:
      return false;
  }
}

String formatSubtitle(Transfer tx) {
  final _txid = tx.txid;
  return [
    _txid,
    ...isIn(tx.type) ? [tx.address] : [],
  ].join('\n\n');
}

class TxData {
  final IconData icon;
  final String title;
  final String summary;
  final bool tappable;
  final bool longPressable;

  const TxData(
    this.icon,
    this.title, {
    this.summary = '',
    this.tappable = false,
    this.longPressable = false,
  });
}

String formatDate(final Transfer tx) {
  final _date = DateTime.fromMillisecondsSinceEpoch(tx.timestamp * 1000);
  return DateFormat.yMMMd().add_jm().format(_date);
}

String formatDateShort(final Transfer tx) {
  final _date = DateTime.fromMillisecondsSinceEpoch(tx.timestamp * 1000);
  return DateFormat.yMMMd().format(_date);
}

String formatDateShorter(final Transfer tx) {
  final _date = DateTime.fromMillisecondsSinceEpoch(tx.timestamp * 1000);
  return DateFormat('d/M/yy').format(_date);
}

String getShortYear(final Transfer tx) {
  final _date = DateTime.fromMillisecondsSinceEpoch(tx.timestamp * 1000);
  return DateFormat('yy').format(_date);
}

String getDay(final Transfer tx) {
  final _date = DateTime.fromMillisecondsSinceEpoch(tx.timestamp * 1000);
  return DateFormat.d().format(_date);
}

String getMonth(final Transfer tx) {
  final _date = DateTime.fromMillisecondsSinceEpoch(tx.timestamp * 1000);
  return DateFormat.MMM().format(_date);
}

String getNumericMonth(final Transfer tx) {
  final _date = DateTime.fromMillisecondsSinceEpoch(tx.timestamp * 1000);
  return DateFormat.M().format(_date);
}

String getYear(final Transfer tx) {
  final _date = DateTime.fromMillisecondsSinceEpoch(tx.timestamp * 1000);
  return DateFormat.y().format(_date);
}

List<TxData> formatDetailIcon(final Transfer tx, final int height) {
  final int _height = tx.height;
  final _accountIndex = tx.subaddrIndex.major;
  final _subaddressIndex = tx.subaddrIndex.minor;
  final int _confirmations = max(height - _height, tx.confirmations);

  bool isUnconfirmed(final TransferType x) {
    switch (x) {
      case TransferType.poolIn:
      case TransferType.pendingOut:
        return true;
      default:
        return false;
    }
  }

  return [
    TxData(
      Icons.receipt,
      tx.txid,
      tappable: true,
      longPressable: true,
    ),
    ...!isIn(tx.type)
        ? []
        : [
            TxData(
              Icons.location_on,
              tx.address,
              tappable: true,
              longPressable: true,
            )
          ],
    ...tx.height == 0
        ? []
        : [
            TxData(
              Icons.straighten,
              _height.toString(),
              tappable: true,
            )
          ],
    TxData(
      Icons.attach_money,
      formatTitle(tx),
    ),
    TxData(
      Icons.credit_card,
      addSymbol(humanizeAmount(tx.fee).toStringAsFixed(4)),
    ),
    ..._accountIndex == 0 && _subaddressIndex == 0
        ? []
        : [
            TxData(
              Icons.account_box,
              '$_accountIndex ➟ $_subaddressIndex',
            )
          ],
    isUnconfirmed(tx.type)
        ? TxData(
            Icons.timelapse,
            ago(tx.timestamp),
          )
        : TxData(
            Icons.access_time,
            formatDate(tx),
          ),
    ...tx.confirmations == 0 || _confirmations > 100
        ? []
        : [
            TxData(
              Icons.confirmation_number,
              _confirmations.toString(),
            )
          ],
  ];
}

String formatAmount(final Transfer tx) {
  return (formatSign(tx) + ' ' + addSymbol(showAmount(tx.amount)).padLeft(10));
}

List<Transfer> sortTxs(final List<Transfer> xs) {
  final _unconfirmed = xs.where((x) => x.height == 0).toList();
  final _confirmed = xs.where((x) => x.height != 0).toList();
  return [
    ..._unconfirmed..sort((x, y) => y.timestamp.compareTo(x.timestamp)),
    ..._confirmed..sort((x, y) => y.height.compareTo(x.height)),
  ];
}

String formatNotification(final Transfer tx) {
  return [
    tx.height.toString(),
    addSymbol(showAmount(tx.amount)),
  ].join('     ');
}
