/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'dart:async';
import 'dart:math';

import '../../logging.dart';

import '../sensor/rpc/wallet.dart' as rpc;
import 'rpc/wallet.dart' as rpc;

Future<String> getUnusedAddress(List<int> used) async {
  log.finest('unused address indices: $used');

  final _usedList = used.toSet().toList()..sort((x, y) => x.compareTo(y));
  log.fine('unused addresses: $_usedList');

  final _usedSet = _usedList.toSet();

  final _options =
      Iterable.generate(200).where((x) => !_usedSet.contains(x)).toList();

  final _largestUsed = _usedList.isEmpty ? 0 : _usedList.last;

  final _nextIndex = _options.isEmpty ? _largestUsed + 1 : _options.first;

  log.fine('next index: $_nextIndex');

  final _addresses = await rpc.getAddresses([_nextIndex]);

  log.fine('addresses: $_addresses');

  if (_addresses.isEmpty) {
    final _newAddress = await rpc.createAddress();
    log.fine('new address: $_newAddress');
    return _newAddress['address'];
  } else {
    return _addresses.first['address'];
  }
}
