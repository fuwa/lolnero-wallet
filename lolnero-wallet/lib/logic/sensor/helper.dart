/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'dart:async';
import 'dart:io';

import 'package:flutter/services.dart';

import 'wallet.dart' as wallet;
import 'daemon.dart' as daemon;

const _methodChannel = const MethodChannel('send-intent');

Future<String> getBinaryDir() async {
  final _dir = await _methodChannel.invokeMethod('getBinaryDir');
  // log.fine('getBinaryDir: ${_dir}');

  // final _binDir = Directory(_dir);
  // final _bins = _binDir.listSync(recursive: true);

  // log.fine('bins: ${_bins}');
  return _dir;
}

Future<String> getBinaryPath(final String name) async {
  final _binaryDir = await getBinaryDir();
  return _binaryDir + '/' + name;
}

Future<bool> isRpcAndDaemonReady() async {
  final _daemonReady = await daemon.isReady();
  final _rpcReady = await wallet.isRpcReady();
  return _daemonReady && _rpcReady;
}

Future<int> getUnsafeUnusedPort() async {
  late int port;
  final socket = await RawServerSocket.bind(InternetAddress.loopbackIPv4, 0);
  port = socket.port;
  await socket.close();
  return port;
}
