/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'package:flutter/material.dart';

class CryptoDaemonConfig {
  final int peerMinimumConnectedTimeInSeconds;
  const CryptoDaemonConfig
  (
    this.peerMinimumConnectedTimeInSeconds,
  );
}

class CryptoConfig {
  final String appPath;
  final String walletRpcBin;
  final String walletCliBin;
  final String opensslBin;
  final ThemeData theme;
  final int port;
  final List<String> extraRpcArgs;
  final List<String> extraCliArgs;
  final List<String> extraCommonArgs;
  final String promptString;
  final int hashViewBlockLength;
  final int daemonPort;
  final int displayShift;
  final int stealthModeDigits;
  final String symbol;
  const CryptoConfig
  (
    this.appPath,
    this.walletRpcBin,
    this.walletCliBin,
    this.opensslBin,
    this.theme,
    this.port,
    this.extraRpcArgs,
    this.extraCliArgs,
    this.extraCommonArgs,
    this.promptString,
    this.hashViewBlockLength,
    this.daemonPort,
    this.displayShift,
    this.stealthModeDigits,
    this.symbol,
  );
}
