/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'package:flutter/material.dart';

import '../../state.dart';

Widget logView(BuildContext context, SyncedState state) {
  final noRpcSymbol = '💤';
  final noRpcWidget = Center(
      child: Text(
    'Wallet RPC: $noRpcSymbol',
    style: Theme.of(context).textTheme.headline3,
  ));

  return OrientationBuilder(
      builder: (context, orientation) => Container(
            child: state.appHook.rpcProcess == null
                ? noRpcWidget
                : Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Expanded(
                          flex: 1,
                          child: SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              reverse: true,
                              child: Text(
                                state.appHook.stdoutCache,
                                style: Theme.of(context).textTheme.bodyText1,
                              ))),
                      orientation != Orientation.portrait
                          ? Container()
                          : Container(
                              margin: const EdgeInsets.all(20.0),
                              child: Container(
                                child: ButtonTheme(
                                  minWidth: 80.0,
                                  height: 80.0,
                                  child: TextButton(
                                    child: Text(
                                      '🚀',
                                      style: TextStyle(fontSize: 50),
                                    ),
                                    onLongPress: state.onStartConsole,
                                    onPressed: () {},
                                  ),
                                ),
                              ),
                            ),
                    ],
                  ),
          ));
}
