/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'package:flutter/material.dart';

import '../state.dart';

Widget build(BuildContext context, SyncingState state) {
  final walletWidget = [
    Spacer(
      flex: 3,
    ),
    LinearProgressIndicator(
      value: state.walletPercent == 0 ? null : state.walletPercent,
      color: Theme.of(context).primaryColor,
    ),
    Spacer(
      flex: 2,
    ),
    Expanded(
        flex: 5,
        child: StreamBuilder(
          stream: state.stdout,
          initialData: '',
          builder: (context, snapshot) => Text(
            snapshot.data.toString(),
            style: Theme.of(context).textTheme.bodyText2,
          ),
        )),
  ];

  return Container(
    padding: const EdgeInsets.all(10.0),
    child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Spacer(
            flex: 10,
          ),
          ...walletWidget,
          Spacer(
            flex: 10,
          ),
        ]),
  );
}
