/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'dart:async';

import '../../../helper.dart';
import '../../../config.dart' as config;
import '../../interface/rpc.dart' as rpc;

Future<dynamic> daemonRpc(final String method,
        {final dynamic params, final String? field}) =>
    rpc.rpc(method, params: params, field: field, daemon: true);

Future<int> targetHeight() =>
    daemonRpc('sync_info', field: 'target_height').then(asInt);
Future<int> height() => daemonRpc('sync_info', field: 'height').then(asInt);

Future<List<Map<String, dynamic>>> getConnectionsSimple() async {
  final _connections = await daemonRpc('get_connections', field: 'connections')
      .then(asJsonArray);

  final _activeConnections = _connections.where((x) =>
      x['live_time'] > config.daemonConfig.peerMinimumConnectedTimeInSeconds);

  final _sortedConn = _activeConnections.toList()
    ..sort((x, y) {
      final int a = x['live_time'];
      final int b = y['live_time'];
      return a.compareTo(b);
    });

  return _sortedConn.toList();
}
