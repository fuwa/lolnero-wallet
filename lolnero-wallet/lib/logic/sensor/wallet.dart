/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'dart:async';
import 'dart:math';
import 'dart:io';

import '../../logging.dart';
import '../../config.dart' as config;
import '../controller/process/run.dart' as run;

import 'rpc/wallet.dart' as rpc;
import 'rpc/daemon.dart' as daemonRpc;

Future<String> walletPath() async {
  final walletDir = await run.getWalletDir();
  return walletDir.path + '/' + config.walletName;
}

Future<String> walletKeyPath() async {
  final walletDir = await run.getWalletDir();
  return walletDir.path + '/' + config.walletName + ".keys";
}

Future<bool> hasWallet() async {
  final _walletKeyPath = await walletKeyPath();

  if (config.isEmu) {
    // return true;
    return false;
  } else {
    log.fine('checking wallet key file: $_walletKeyPath');
    return File(_walletKeyPath).exists();
  }
}

Future<bool> isSynced() async {
  final _walletHeight = await rpc.getHeight();
  final _daemonHeight = await daemonRpc.height();
  return (_daemonHeight == _walletHeight) && (_walletHeight != 0);
}

Future<bool> isRpcReady() async {
  return await rpc.getVersion() > 0;
}
