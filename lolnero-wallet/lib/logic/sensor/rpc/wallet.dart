/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'dart:async';

import '../../../helper.dart';
import '../../interface/rpc.dart';
import '../../data/rpc/typed/transfer.dart';

Future<int> getVersion() => rpc('get_version', field: 'version').then(asInt);

Future<int> getHeight() => rpc('get_height', field: 'height').then(asInt);

// Future<int> getBalance() => rpc('get_height', field: 'height').then(asInt);

Future<Map<String, dynamic>> getAccounts() => rpc('get_accounts').then(asMap);

Future<List<Map<String, dynamic>>> getSubaddresssAccounts() => getAccounts()
    .then((x) => x['subaddress_accounts'])
    .then(asList)
    .then((x) => x.cast<Map<String, dynamic>>());

Future<int> getTotalUnlockedBalance() =>
    rpc('get_accounts', field: 'total_unlocked_balance').then(asInt);

Future<Map<String, dynamic>> getTransfers(final List<String> txTypes) {
  final params = {for (final v in txTypes) v: true};
  return rpc('get_transfers', params: params).then(asMap);
}

// minHeight is exclusive, maxHeight is inclusive
Future<Map<TransferType, List<Transfer>>> getTransfersFor(
  final List<TransferType> txTypes, {
  final int minHeight = 0,
  final int? maxHeight,
  final int accountIndex = 0,
  final bool allAccounts = true,
}) async {
  final params = {
    ...{for (final v in txTypes) showTransferType(v): true},
    ...{
      'min_height': minHeight,
      'max_height': maxHeight,
      'filter_by_height': true,
      'account_index': accountIndex,
      'all_accounts': allAccounts,
    },
  };

  // log.fine('params: $params');
  final transfers = await rpc('get_transfers', params: params).then(asMap);
  final typedTransfers = {
    for (final txType in txTypes)
      txType: asList(transfers[showTransferType(txType)])
          .map((x) => Transfer.fromJson(x))
          .toList()
  };

  return typedTransfers;
}

Future<List<Map<String, dynamic>>> getAddresses(
  final List<int> addressIndices,
) async {
  final params = {
    'account_index': 0,
    'address_index': addressIndices,
  };

  // log.fine('params: $params');
  final address = await rpc('get_address', params: params).then(asMap);
  return asList(address['addresses']).cast<Map<String, dynamic>>();
}

Future<String> getSpendProof(final String x) =>
    rpc('get_spend_proof', params: {'txid': x}, field: 'signature')
        .then(asString);

Future<bool> checkSpendProof(final String txid, final String x) => rpc(
      'check_spend_proof',
      params: {
        'txid': txid,
        'signature': x,
      },
      field: 'good',
    ).then(asBool);
