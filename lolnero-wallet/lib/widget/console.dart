/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../state.dart';
import '../logging.dart';

Widget build(BuildContext context, ConsoleState state) {
  final _consoleInput = TextFormField(
    controller: state.textController,
    // textInputAction: TextInputAction.next,
    autocorrect: false,
    enableSuggestions: false,
    keyboardType: TextInputType.visiblePassword,
    decoration: InputDecoration(
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(
          color: Theme.of(context).primaryColor,
        ),
      ),
      border: InputBorder.none,
    ),
    onFieldSubmitted: (v) {
      final _text = state.textController.text.trim();
      final line = _text;

      log.finer('terminal input: $line');
      state.appendInput(line);
      state.textController.clear();
    },
  );

  return Container(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Expanded(
            flex: 1,
            child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                reverse: true,
                child: SelectableText(
                  state.appHook.stdoutCache,
                  style: Theme.of(context).textTheme.bodyText1,
                ))),
        Container(
          margin: const EdgeInsets.all(10.0),
          child: _consoleInput,
        ),
      ],
    ),
  );
}
