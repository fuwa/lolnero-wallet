/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

/*
import 'dart:async';
import 'dart:core';

import '../logging.dart';
import '../logic/data/rpc/typed/transfer.dart';
import '../logic/sensor/rpc/wallet.dart' as rpc;
import '../logic/data/spendProof.dart';

import 'prototype.dart';

typedef NextSyncedStateFunc = Future<AppStateAutomata> Function(
    SpendProof spendProof);

class VerifyingState extends AppStateAutomata {
  final NextSyncedStateFunc getNextState;
  final List<Transfer> txs;
  final String signature;
  final int findIndex;

  VerifyingState(
    appHook,
    this.getNextState,
    this.txs,
    this.signature,
    this.findIndex,
  ) : super(appHook);

  Future<AppStateAutomata> next() async {
    if (await shouldExit()) return exitState();
    log.fine('verifying state $findIndex');

    final _tx = txs[findIndex];
    final _found = await rpc.checkSpendProof(_tx.txid, signature);

    if (_found) {
      log.fine('found spend proof for tx: ${_tx.txid}');
      final _checkedSpendProof = SpendProof(
        _tx.txid,
        signature,
      );
      final _nextState = await getNextState(_checkedSpendProof);
      return _nextState;
    }

    final _nextIndex = findIndex + 1;
    if (_nextIndex >= txs.length) {
      final _nextState = await getNextState(null);
      return _nextState;
    } else {
      return VerifyingState(
        appHook,
        getNextState,
        txs,
        signature,
        _nextIndex,
      );
    }
  }
}
*/