/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'package:flutter/material.dart';

import '../../state.dart';
import '../../helper.dart';
import '../../config.dart' as config;
import '../../logic/data/transfer.dart' as transfer;
import '../../logic/data/format.dart';
import '../snippet/tx.dart' as tx;

Widget defaultView(BuildContext context, SyncedState state) {
  final _balance = showStealthAmount(state.unlockedBalance);
  final _hotTxs = transfer.sortTxs(state.getPool());

  final _txList = Expanded(
    child: Container(
      margin: const EdgeInsets.only(top: 40, bottom: 40),
      child: tx.txListView(Theme.of(context), state, [
        ..._hotTxs,
        // ... state.getIn(),
      ]),
    ),
  );

  final _txView = Column(
    mainAxisAlignment: MainAxisAlignment.center,
    mainAxisSize: MainAxisSize.max,
    children: <Widget>[
      Spacer(),
      Text(
        addSymbol('$_balance'),
        style: Theme.of(context).textTheme.headline4,
      ),
      _txList,
    ],
    key: Key('tx'),
  );

  final _refreshView = Column(
    mainAxisAlignment: MainAxisAlignment.center,
    mainAxisSize: MainAxisSize.max,
    children: <Widget>[
      Spacer(),
      CircularProgressIndicator(
        color: Theme.of(context).primaryColor,
      ),
      Spacer(),
    ],
    key: Key('refresh'),
  );

  return Container(
    child: AnimatedSwitcher(
      duration: config.transitionDurationShort,
      child: state.refreshed ? _txView : _refreshView,
    ),
  );
}
