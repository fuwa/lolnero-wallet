/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'package:flutter/material.dart';

import 'prototype.dart';

const crtGreen = Color.fromRGBO(51, 255, 51, 1);

final _theme = ThemeData(
  brightness: Brightness.dark,
  primaryColor: crtGreen,
  hintColor: Colors.yellow,
  accentColor: crtGreen,
  backgroundColor: Colors.black,
  scaffoldBackgroundColor: Colors.black,
  textSelectionTheme: TextSelectionThemeData(
    cursorColor: crtGreen,
    selectionColor: crtGreen,
    selectionHandleColor: crtGreen,
  ),
  textButtonTheme: TextButtonThemeData(
    style: TextButton.styleFrom(primary: crtGreen),
  ),
  textTheme: TextTheme(
    headline4: TextStyle(
      fontFamily: 'RobotoMono',
      fontSize: 35,
      fontWeight: FontWeight.bold,
    ),
    headline3: TextStyle(
      fontFamily: 'RobotoMono',
      fontSize: 22,
    ),
    headline6: TextStyle(
      fontFamily: 'VT323',
      fontSize: 22,
    ),
    subtitle1: TextStyle(
      fontFamily: 'RobotoMono',
      fontSize: 17,
      fontWeight: FontWeight.bold,
    ),
    bodyText2: TextStyle(
      fontFamily: 'VT323',
      fontSize: 16,
      height: 1,
    ),
    bodyText1: TextStyle(
      fontFamily: 'RobotoMono',
      fontSize: 12,
    ),
  ).apply(
    bodyColor: crtGreen,
    displayColor: crtGreen,
  ),
);

final int _rpcPort = 44444;

final daemonConfig = CryptoDaemonConfig(
    2,
);

final config = CryptoConfig(
    'lolnero',
    'liblolnero-rpc.so',
    'liblolnero.so',
    'libopenssl.so',
    _theme,
    _rpcPort,
    [
      '--rpc-bind-port=$_rpcPort',
    ],
    [],
    [],
    '[lolnero]: ',
    6,
    45679,
    11,
    5,
    'λ',
);
