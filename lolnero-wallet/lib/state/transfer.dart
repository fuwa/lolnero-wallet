/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'dart:async';

import 'package:flutter/material.dart';

import '../logging.dart';
import '../helper.dart';
import '../config.dart' as config;
import '../logic/controller/rpc/wallet.dart' as rpc;
import '../logic/sensor/helper.dart' as sensor;
import '../logic/data/uri.dart' as uri;

import 'prototype.dart';
import 'synced.dart';
import 'receipt.dart';
import 'resyncing.dart';

class TransferState extends AppStateAutomata {
  final List<Map<String, dynamic>> accounts;
  final String address;
  final uri.PaymentInfo paymentInfo;
  final GlobalKey<FormState> formKey;
  final TxCache txCache;

  int accountIndex;

  double amount;
  bool canceled = false;
  bool sent = false;

  TransferState(
    appHook,
    this.accounts,
    this.address,
    this.paymentInfo,
    this.formKey,
    this.txCache,
    this.accountIndex,
    this.amount,
  ) : super(appHook);

  void onSend() {
    log.fine('on send');
    sent = true;
    syncState();
  }

  void onCancel() {
    log.fine('on cancel');
    canceled = true;
  }

  void onSetAccountIndex(final String value) {
    log.fine('on SetAccountIndex');
    final parsed = int.tryParse(value) ?? 0;
    accountIndex = (parsed < accounts.length && parsed >= 0) ? parsed : 0;
  }

  void onChangeAmount(final String value) {
    log.fine('on ChangeAmount $value');

    amount = double.tryParse(value) ?? 0;

    final _currentBalance =
        humanizeAmount(accounts[accountIndex]['unlocked_balance']);
    double safeBalance(x) => x - config.reservedFee;

    final _currentSafeBalance = safeBalance(_currentBalance);

    final _usableAccounts = accounts.where(
        (x) => safeBalance(humanizeAmount(x['unlocked_balance'])) >= amount);

    final _nonEmptyAmount = amount > 0;
    final _invalidAccount = _currentSafeBalance < amount;

    if (_nonEmptyAmount && _invalidAccount && (_usableAccounts.isNotEmpty)) {
      accountIndex = _usableAccounts.first['account_index'];
      log.info('auto selecting account index: $accountIndex');
      syncState();
    }
  }

  Future<AppStateAutomata> next() async {
    if (await shouldExit()) return exitState();

    if (canceled) {
      return SyncedState.cachedSyncedState(appHook, txCache);
    }
    if (sent) {
      if (!await sensor.isRpcAndDaemonReady()) {
        return ResyncingState(appHook, () async {
          await tick();
          await rpc.refresh();
          await tick();

          return this;
        });
      }

      final _result = await rpc.transferSplit(
          address, digitizeAmount(amount), accountIndex);
      log.fine('transfer result: $_result');

      final List<int> _amounts = asList(_result['amount_list']).cast<int>();
      final List<int> _fees = asList(_result['fee_list']).cast<int>();
      final List<String> _txHashes =
          asList(_result['tx_hash_list']).cast<String>();

      return ReceiptState(appHook, _amounts, _fees, _txHashes, txCache);
    } else {
      await waitInput();
      return this;
    }
  }
}
