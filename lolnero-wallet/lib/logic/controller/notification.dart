/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'package:shared_preferences/shared_preferences.dart';

import 'dart:async';

import '../../logging.dart';
import '../../config.dart' as config;

import '../data/rpc/typed/transfer.dart';
import '../data/transfer.dart' as transfer;

Future<List<Transfer>> getNewTxs(
  final int height,
  final List<Transfer> txs,
) async {
  if (height <= 0) return [];

  SharedPreferences _prefs = await SharedPreferences.getInstance();
  final _lastHeight = _prefs.getInt(config.notificationHeightKey);
  // final _lastHeight = 178600;

  if (_lastHeight == null) {
    log.fine('new wallet, setting notification height: $height');
    _prefs.setInt(config.notificationHeightKey, height);

    return [];
  }

  if (_lastHeight == height) {
    log.fine('no new txs for notification: $height');
    return [];
  }

  if (_lastHeight > height) {
    log.severe(
        'notification height $_lastHeight > current cached height $height');

    if (height > 0) {
      await _prefs.setInt(config.notificationHeightKey, height);
      log.fine('saving notification height: $height');
    }
    return [];
  }

  log.fine('finding txs with height > $_lastHeight');
  final newTxs = txs.where((x) => x.height > _lastHeight).toList();

  log.fine('found: $newTxs');

  await _prefs.setInt(config.notificationHeightKey, height);
  log.fine('saving notification height: $height');

  return transfer.sortTxs(newTxs);
}
