/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'package:flutter/material.dart';

import '../state.dart';

Widget build(BuildContext context, CheckWalletState state) {
  return Container(
    padding: const EdgeInsets.all(40.0),
    child: Center(
      child: state.walletGenerated
          ? CircularProgressIndicator(
            color: Theme.of(context).primaryColor,
          )
          : !state.seedGenerated
              ? Container()
              : ListTile(
                  leading: Icon(
                    Icons.flag,
                    size: 32,
                    // TRIADIC 1
                    color: Color(0xffd4ff5f),
                  ),
                  title: Padding(
                    padding: EdgeInsets.fromLTRB(0, 16, 0, 16),
                    child: Text(
                      'Intro Quest (${state.countDown})',
                      style: Theme.of(context).textTheme.headline6,
                    ),
                  ),
                  subtitle: Text(
                    'Send a seed here to prove your worth.',
                    style: Theme.of(context).textTheme.headline6,
                  ),
                ),
    ),
  );
}
