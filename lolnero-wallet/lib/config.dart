/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'config/lolnero.dart' as cryptoConfig;

final c = cryptoConfig.config;
final daemonConfig = cryptoConfig.daemonConfig;

enum Arch { arm64, x86_64 }

const arch = Arch.arm64;
// const arch = Arch.x86_64;
const minimumHeight = 4000;

const isEmu = identical(arch, Arch.x86_64);
const emuHost = '192.168.10.170';

const host = '127.0.0.1';
const daemonHost = host;

// const host = emuHost;
// const daemonHost = emuHost;

const walletName = 'lolnero';

const stdoutLineBufferSize = 100;
const reorgBuffer = 4;
const forcedUpdateInSeconds = 300;
const rpcPullIntervalInMilliseconds = 4000;
const int targetInputLagInMilliseconds = 8;

const double reservedFee = 0.1;
const notificationHeightKey = 'notification-height';
const walletImportedKey = 'wallet-imported';

const paymentMethodName = 'address';
const paymentParamAmount = 'amount';
const paymentParamLabel = 'label';
const paymentParamMessage = 'message';

const searchMethodName = 'search';
const searchParamKeyword = 'keyword';

const int searchViewIndex = 1;
const int defaultViewIndex = 2;

const transitionDuration = const Duration(milliseconds: 400);
const transitionDurationShort = const Duration(milliseconds: 240);

const searchDetailSummaryLength = 64;

const int rpcTimeoutInSeconds = 4;
const int rpcLongWaitingTimeoutInSeconds = 16;
const int rpcKillWaitingInSeconds = 2;

const int logLines = 200;
const int exitLogoDelayInMilliseconds = 600;

const int countDown = 3;
