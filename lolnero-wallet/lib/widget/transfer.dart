/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'dart:core';

import 'package:flutter/material.dart';

import '../helper.dart';
import '../state.dart';
import '../logic/data/format.dart';
import '../config.dart' as config;

Widget build(BuildContext context, TransferState state) {
  final _address = state.address;
  final _currentBalance =
      humanizeAmount(state.accounts[state.accountIndex]['unlocked_balance']);
  double safeBalance(x) => x - config.reservedFee;

  final _validAmount =
      state.amount > 0 && state.amount < safeBalance(_currentBalance);
  // log.fine('state amount: ${state.amount}');
  // log.fine('currentBalance: ${_currentBalance}');
  // log.fine('validamount: ${_validAmount}');

  final _initialAmount = state.paymentInfo.amount;

  final _label = state.paymentInfo.label.isEmpty
      ? []
      : [
          Spacer(
            flex: 1,
          ),
          Text(
            state.paymentInfo.label,
            style: Theme.of(context).textTheme.headline4,
          ),
        ];

  final _message = state.paymentInfo.message.isEmpty
      ? []
      : [
          Spacer(
            flex: 1,
          ),
          Text(
            state.paymentInfo.message,
            style: Theme.of(context).textTheme.headline6,
          ),
        ];

  final _form = Form(
    key: state.formKey,
    child: Column(
      children: <Widget>[
        // Text (
        //   'amount:',
        //   style: Theme.of(context).textTheme.subhead,
        // ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          // padding: const EdgeInsets.all(20.0),
          child: TextFormField(
            textAlign: TextAlign.center,
            initialValue:
                _initialAmount == 0.0 ? '' : _initialAmount.toString(),
            onChanged: state.onChangeAmount,
            style: Theme.of(context).textTheme.headline4,
            autofocus: true,
            keyboardType: TextInputType.number,
            // ignore: deprecated_member_use
            autovalidate: true,
            validator: (value) {
              if (value == null) {
                // return 'Please enter some amount';
                return null;
              }
              if (value.isEmpty) {
                // return 'Please enter some amount';
                return null;
              }

              final double? _amount = double.tryParse(value);
              if (_amount == null) {
                return 'Please enter a valid amount';
              }

              if (_amount == 0) {
                // 0 is acceptable, but send will still be disabled
                return null;
              }

              if (_amount < 0) {
                return 'Please enter a positive amount';
              }

              if (_amount > _currentBalance) {
                return 'Only ${addSymbol(formatAmount(_currentBalance))} spendable';
              }

              return null;
            },
          ),
        ),
        ButtonBar(
          mainAxisSize: MainAxisSize.max,
          alignment: MainAxisAlignment.spaceBetween,
          // buttonTextTheme: ButtonTextTheme.primary,
          children: <Widget>[
            TextButton(
              child: Text('Cancel'),
              onPressed: state.onCancel,
            ),
            TextButton(
              child: Text('Send'),
              onPressed: _validAmount ? state.onSend : null,
            ),
          ],
        ),
      ],
    ),
  );

  return Container(
    padding: const EdgeInsets.all(10.0),
    child: state.sent
        ? Center(
            child: CircularProgressIndicator(
              color: Theme.of(context).primaryColor,
            ),
          )
        : Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
                ..._label,
                Spacer(
                  flex: 1,
                ),
                Text(
                  '$_address',
                  style: Theme.of(context).textTheme.subtitle1,
                ),
                ..._message,
                Spacer(
                  flex: 2,
                ),
                _form,
              ]),
  );
}
