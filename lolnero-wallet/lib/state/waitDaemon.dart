/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'dart:async';
import 'dart:core';

import '../logging.dart';
import '../helper.dart';

import '../logic/sensor/daemon.dart' as daemon;

import 'prototype.dart';

class WaitDaemonState extends AppStateAutomata {
  final AppStateAutomata pushedState;

  WaitDaemonState(appHook, this.pushedState) : super(appHook);

  Future<AppStateAutomata> next() async {
    if (await shouldExit()) return exitState();

    log.fine('wait daemon state next');

    await tick();

    final _daemonReady = await daemon.isReady();
    if (_daemonReady) {
      log.fine('daemon synced');
      return pushedState;
    } else {
      log.fine('daemon not synced, waiting ...');
      return this;
    }
  }
}
