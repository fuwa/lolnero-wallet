/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'package:path_provider/path_provider.dart';
import 'package:flutter/foundation.dart';

import 'dart:io';
import 'dart:async';

import '../../../logging.dart';
import '../../sensor/helper.dart';
import '../../../config.dart' as config;

typedef ShouldExit = bool Function();
typedef SetKillRpcProcess = void Function(bool Function());

const String walletDirName = 'wallet';
const String logDirName = 'log';
const String logName = 'wallet-rpc.log';

Future<Directory> getWorkingDir() async {
  final appDocDir = await getApplicationDocumentsDirectory();
  return Directory(appDocDir.path + "/" + config.c.appPath);
}

Future<Directory> getWalletDir() async {
  final workingDir = await getWorkingDir();
  return Directory(workingDir.path + '/' + walletDirName);
}

Future<Directory> getLogDir() async {
  final workingDir = await getWorkingDir();
  return Directory(workingDir.path + '/' + logDirName);
}

Future<Process> runBinary(
  final String name,
  final List<String> args,
) async {
  final binaryPath = await getBinaryPath(name);
  log.fine('run binary: $binaryPath');

  final walletDir = await getWalletDir();
  await walletDir.create(recursive: true);

  // final logDir = await getLogDir();
  // await logDir.create(recursive: true);

  const List<String> debugArgs = [];
  const List<String> releaseArgs = [];

  const extraBuildFlavorArgs = kReleaseMode ? releaseArgs : debugArgs;

  final _args = [
    ...args,
    ...config.c.extraCommonArgs,
    ...extraBuildFlavorArgs,
  ];

  log.info('args: ' + _args.toString());

  if (config.isEmu) {
    log.fine('run emu');
    await Process.start('echo', ['hello world']);
  }

  final _process = await Process.start(
    binaryPath,
    _args,
    workingDirectory: walletDir.path,
  );

  return _process;
}
