/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'dart:async';

import '../../../helper.dart';
import '../../interface/rpc.dart';

Future<Map<String, dynamic>> restoreFromSeed(final params) =>
    rpc('restore_deterministic_wallet', params: params).then(asMap);

Future<void> store() => rpc('store');
Future<void> save() => rpc('save');
Future<Map<String, dynamic>> refresh() => rpc('refresh').then(asMap);

Future<String> openWallet(final params) =>
    rpcString('open_wallet', params: params);

Future<Map<String, dynamic>> transferSplit(
  final String address,
  final int amount,
  final int accountIndex,
) async {
  final params = {
    'destinations': [
      {
        'address': address,
        'amount': amount,
      },
    ],
    'account_index': accountIndex,
    'get_tx_hex': true,
    'ring_size': 22,
  };

  // log.fine('params: $params');
  return rpc(
    'transfer_split',
    params: params,
    longWaiting: true,
  ).then(asMap);
}

Future<Map<String, dynamic>> createAddress() async {
  final params = {
    'account_index': 0,
  };

  return rpc('create_address', params: params).then(asMap);
}
