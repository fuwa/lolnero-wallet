/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

int asInt(dynamic x) => x?.toInt() ?? 0;

bool asBool(dynamic x) => x ?? false;

List<dynamic> asList(dynamic x) => x ?? [];
List<Map<String, dynamic>> asJsonArray(dynamic x) =>
    x?.cast<Map<String, dynamic>>() ?? [];

Map<String, dynamic> asMap(dynamic x) => x ?? {};
String asString(dynamic x) => x ?? '';
