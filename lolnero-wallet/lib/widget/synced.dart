/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../state.dart';
import '../config.dart' as config;

import 'synced/defaultView.dart';
import 'synced/searchView.dart';
import 'synced/qrView.dart';
import 'synced/logView.dart';

Widget pageView(BuildContext context, SyncedState state) {
  void _onPageChanged(int pageIndex) {
    if (pageIndex != config.searchViewIndex) {
      SystemChannels.textInput.invokeMethod('TextInput.hide');
    }
  }

  return PageView(
    onPageChanged: _onPageChanged,
    controller: state.pageController,
    children: [
      logView(context, state),
      searchView(context, state),
      defaultView(context, state),
      qrView(context, state),
    ],
  );
}

Widget build(BuildContext context, SyncedState state) {
  return pageView(context, state);
}
