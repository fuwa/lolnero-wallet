/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'package:flutter/material.dart';

import '../../state.dart';
import '../../config.dart' as config;
import '../snippet/tx.dart' as tx;

Widget searchView(
  final BuildContext context,
  final SyncedState state,
) {
  final _searchResult = Expanded(
    flex: 10,
    child: AnimatedSwitcher(
      duration: config.transitionDurationShort,
      child: tx.searchListView(
        Theme.of(context),
        state,
        state.searchCache.searchResult,
      ),
    ),
  );

  final _searchField = TextFormField(
    textAlign: TextAlign.center,
    autocorrect: false,
    enableSuggestions: false,
    keyboardType: TextInputType.visiblePassword,
    // onChanged: state.onChangeSearch,
    onFieldSubmitted: state.onChangeSearch,
    controller: state.textEditingController,
    style: Theme.of(context).textTheme.headline4,
  );

  return Container(
    padding: const EdgeInsets.all(10.0),
    child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: _searchField,
          ),
          _searchResult,
        ]),
  );
}
