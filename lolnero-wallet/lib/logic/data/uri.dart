/*

Copyright 2020 fuwa


This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'dart:math';
import 'dart:core';

import '../../logging.dart';
import '../../helper.dart';
import '../../config.dart' as config;
import '../../logic/data/rpc/typed/transfer.dart';

import 'wallet.dart' as wallet;
import 'transfer.dart' as transfer;

class PaymentInfo {
  final bool hasAmount;
  final double amount;
  final String label;
  final String message;

  const PaymentInfo(this.hasAmount, this.amount, this.label, this.message);
}

const defaultPaymentInfo = const PaymentInfo(false, 0.0, '', '');

enum IntentType {
  empty,
  payment,
  search,
}

class Payment {
  final String address;
  final PaymentInfo paymentInfo;

  const Payment(
    this.address,
    this.paymentInfo,
  );
}

class Search {
  final String keyword;

  const Search(
    this.keyword,
  );
}

class SendData {
  final IntentType intentType;
  final Payment? payment;
  final Search? search;

  const SendData(
    this.intentType,
    this.payment,
    this.search,
  );
}

const defaultSendData = const SendData(
  IntentType.empty,
  null,
  null,
);

SendData parseUri(final List<Transfer> txs, final String x) {
  if (x.isEmpty) return defaultSendData;

  final _rawAddress = x;
  if (wallet.isAddress(_rawAddress)) {
    return SendData(
      IntentType.payment,
      Payment(
        _rawAddress,
        defaultPaymentInfo,
      ),
      null,
    );
  }

  final _uri = Uri.tryParse(x);
  if (_uri == null) {
    return defaultSendData;
  }

  log.fine('uri: $_uri');

  final _params = _uri.queryParameters;
  final _address = asString(_params['address']);

  if (wallet.isAddress(_address)) {
    log.fine('address: $_address');
    final _maybeAmount = _params['amount'] == null
        ? null
        : double.tryParse(_params['amount'] ?? '');

    final _hasAmount = _maybeAmount != null;
    final _amount = _maybeAmount ?? 0.0;
    log.fine('amount: $_amount');

    final _label = _params['label'] ?? '';
    log.fine('label: $_label');

    final _message = _params['message'] ?? '';
    log.fine('message: $_message');

    final _paymentInfo = PaymentInfo(
      _hasAmount,
      _amount,
      _label.trim(),
      _message.trim(),
    );

    return SendData(
      IntentType.payment,
      Payment(
        _address,
        _paymentInfo,
      ),
      null,
    );
  }

  if (_params['method'] == config.searchMethodName) {
    // log.fine('searching api called');

    final _keyword = asString(_params[config.searchParamKeyword]).trim();
    // log.fine('keyword: ${_keyword}');

    if (_keyword.isNotEmpty) {
      return SendData(
        IntentType.search,
        null,
        Search(
          _keyword,
        ),
      );
    }
  }

  if (x.length >= 5) {
    final _searchResult = transfer.searchTransferMulti(txs, [x]);

    if (_searchResult.isNotEmpty) {
      return SendData(
        IntentType.search,
        null,
        Search(
          x,
        ),
      );
    }
  }

  return defaultSendData;
}
