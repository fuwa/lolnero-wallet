/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'dart:async';
import 'dart:core';

import '../logging.dart';
import '../helper.dart';
import '../logic/controller/rpc/wallet.dart' as rpc;
import '../logic/sensor/daemon.dart' as daemon;
import '../logic/sensor/wallet.dart' as wallet;

import 'prototype.dart';
import 'waitDaemon.dart';

typedef NextStateFunc = Future<AppStateAutomata> Function();

class ResyncingState extends AppStateAutomata {
  final NextStateFunc getNextState;
  bool walletSynced = false;

  ResyncingState(appHook, this.getNextState) : super(appHook);

  Future<AppStateAutomata> next() async {
    if (await shouldExit()) return exitState();

    log.fine('wait daemon state next');

    final _daemonReady = await daemon.isReady();
    if (!_daemonReady) {
      return WaitDaemonState(appHook, this);
    }

    while (true) {
      if (await shouldExit()) return exitState();

      if (await shouldSkip()) {
        log.finest('skipping state update');
        continue;
      }

      if (!await wallet.isRpcReady()) {
        return await reset();
      }

      final _walletSynced = await wallet.isSynced();

      if (_walletSynced) {
        log.fine('wallet synced');
        walletSynced = true;
        break;
      }

      // log.info('Resyncing: wallet ${_walletHeight} / daemon ${_daemonHeight}');

      await tick();
      await rpc.refresh();
    }

    await rpc.store();

    final _nextState = await getNextState();
    return _nextState;
  }
}
