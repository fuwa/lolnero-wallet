/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'package:shared_preferences/shared_preferences.dart';

import 'dart:async';

import '../../config.dart' as config;

Future<bool> walletImported() async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  return _prefs.getBool(config.walletImportedKey) ?? false;
}

Future<void> setWalletImported(bool flag) async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  await _prefs.setBool(config.walletImportedKey, flag);
}
