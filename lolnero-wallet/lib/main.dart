/*

Copyright 2020 fuwa

This file is part of Wowllet.


Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/foundation.dart';
import 'package:logging/logging.dart';
import 'package:intl/date_symbol_data_local.dart';

import 'dart:io';
import 'dart:async';
import 'dart:collection';

import 'logging.dart';
import 'config.dart' as config;
import 'state.dart' as state;
import 'widget.dart' as widget;

void main() {
  Logger.root.level = kReleaseMode ? Level.INFO : Level.FINE;
  Logger.root.onRecord.listen((LogRecord rec) {
    print('${rec.level.name}: ${rec.time}: ${rec.message}');
  });

  log.fine('flutter main');

  initializeDateFormatting().then((_) => runApp(LolneroWalletApp()));
}

class LolneroWalletApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
    log.fine('Wowllet_App build');

    return MaterialApp(
      title: 'Lolnero Wallet',
      theme: config.c.theme,
      darkTheme: config.c.theme,
      home: LolneroWalletPage('Lolnero Wallet'),
    );
  }
}

class LolneroWalletPage extends StatefulWidget {
  final String title;
  LolneroWalletPage(this.title, {Key? key}) : super(key: key);

  @override
  LolneroWalletPageState createState() {
    log.fine('LolneroWalletPageState createState');
    return LolneroWalletPageState();
  }
}

class LolneroWalletPageState extends State<LolneroWalletPage>
    with WidgetsBindingObserver {
  static const _channel = const MethodChannel('send-intent');
  Queue<String> _intent = Queue();

  state.AppStateAutomata? _state;
  AppLifecycleState _notification = AppLifecycleState.resumed;
  FocusNode _focusNode = FocusNode();

  bool _exiting = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  Future<String> getInitialIntent() async {
    final text = await _channel.invokeMethod('getInitialIntent');
    log.fine('getInitialIntent: $text');
    return text.trim();
  }

  @override
  void didChangeAppLifecycleState(final AppLifecycleState state) {
    log.fine('app cycle: $state');
    setState(() {
      _notification = state;
    });
  }

  void _setState(final state.AppStateAutomata newState) {
    setState(() => _state = newState);
  }

  String _getIntent() {
    return _intent.isEmpty ? '' : _intent.removeFirst();
  }

  bool _hasIntent() => _intent.isNotEmpty;

  Future<void> buildStateMachine(final state.BlankState _blankState) async {
    _setState(_blankState);

    bool exited = false;
    bool validState = true;

    final _initialIntent = await getInitialIntent();
    if (_initialIntent.isNotEmpty) {
      _intent.add(_initialIntent);
    }

    while (validState && !exited) {
      switch (_state.runtimeType) {
        case state.ExitingState:
          {
            await (_state as state.ExitingState).wait();
            log.finer('exit state wait done');
            exited = true;
          }
          break;

        case state.BlankState:
        case state.LoadingState:
        case state.CheckWalletState:
        case state.SyncingState:
        case state.SyncedState:
        case state.TransferState:
        case state.ReceiptState:
        case state.WaitDaemonState:
        case state.ResyncingState:
        case state.LoadConsoleState:
        case state.ConsoleState:
          _setState(await (_state as state.AppStateAutomata).next());
          break;

        default:
          validState = false;
      }
    }

    log.finer('state machine finished');

    if (exited) {
      log.finer('popping navigator');
      exit(0);
    } else {
      log.severe('Reached invalid state!');
      exit(1);
    }
  }

  @override
  void initState() {
    super.initState();
    log.fine("Wowllet_PageState initState");

    WidgetsBinding.instance?.addObserver(this);

    _focusNode = FocusNode();
    _focusNode.attach(context);

    Future<dynamic> handleMethod(MethodCall f) async {
      log.fine('handle send message in flutter');
      log.fine(f.method);
      log.fine(f.arguments);
      log.fine('arguments type: ${f.arguments.runtimeType}');

      switch (f.method) {
        case 'send':
          _intent.add(f.arguments.trim());
          return null;
        default:
        // todo - throw not implemented
      }
    }

    _channel.setMethodCallHandler(handleMethod);

    buildStateMachine(getBlankState());
  }

  state.BlankState getBlankState() {
    final state.AppHook _appHook = state.AppHook(
      (x) => setState(() => _state = x as state.AppStateAutomata),
      () => _notification,
      () => _exiting,
      getInitialIntent,
      _getIntent,
      _hasIntent,
      () => _focusNode,
      () => _scaffoldKey,
      null,
      Queue(),
    );

    return state.BlankState(_appHook);
  }

  Future<bool> _exitApp(final BuildContext context) async {
    if (_state != null) {
      if (!_state!.shouldPop()) return false;
    }
    log.info("Wowllet_PageState _exitApp");
    WidgetsBinding.instance?.removeObserver(this);

    _exiting = true;

    await Future.delayed(const Duration(seconds: 5), () => null);

    // the process controller should call exit(0) for us
    log.warning('Wallet server took too long to shut down!');
    exit(1);
  }

  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    _focusNode.dispose();

    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    return WillPopScope(
      onWillPop: () => _exitApp(context),
      child: widget.build(context, _state!),
    );
  }
}
