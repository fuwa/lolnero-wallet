/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'dart:async';
import 'dart:math';
import 'dart:collection';

import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:share_plus/share_plus.dart' as share;
import 'package:qr_flutter/qr_flutter.dart' as qr;

import '../config.dart' as config;
import '../helper.dart';
import '../logging.dart';

import '../logic/controller/notification.dart' as notification;
import '../logic/controller/rpc/wallet.dart' as rpc;
import '../logic/controller/wallet.dart' as wallet;
import '../logic/data/rpc/typed/transfer.dart';
import '../logic/data/transfer.dart' as transfer;
import '../logic/data/uri.dart' as uri;
import '../logic/sensor/rpc/wallet.dart' as rpc;
import '../logic/sensor/txCache.dart' as cache;
import '../logic/sensor/wallet.dart' as wallet;
import '../logic/sensor/helper.dart' as sensor;

import 'prototype.dart';
import 'transfer.dart';
import 'resyncing.dart';
import 'loadConsole.dart';

class SearchCache {
  List<Transfer> searchResult;
  Queue<String> history;

  SearchCache(
    this.history, {
    this.searchResult = const [],
  });
}

class SyncedState extends AppStateAutomata {
  final int unlockedBalance;
  final int height;
  final TxCache txCold;
  final TxCache txHot;
  final TxCache txPool;
  final String address;
  final PageController pageController;
  final TextEditingController textEditingController;
  final SearchCache searchCache;
  final qr.QrImage? qrImage;
  final int rpcPullTimestampInMilliseconds;
  final bool refreshed;
  bool searchNeedsUpdate;
  bool startConsole;

  SyncedState(
    appHook,
    this.height,
    this.unlockedBalance,
    this.txCold,
    this.txHot,
    this.txPool,
    this.address,
    this.pageController,
    this.textEditingController,
    this.searchCache, {
    this.qrImage,
    this.rpcPullTimestampInMilliseconds = 0,
    this.refreshed = false,
    this.searchNeedsUpdate = false,
    this.startConsole = false,
  }) : super(appHook);

  factory SyncedState.cachedSyncedState(
    AppHook appHook,
    TxCache txCold,
  ) =>
      SyncedState(
        appHook,
        0,
        0,
        txCold,
        TxCache.txCacheAt(txCold.height),
        defaultTxCache,
        '',
        PageController(initialPage: config.defaultViewIndex),
        TextEditingController(),
        SearchCache(Queue()),
      );

  void onShare(final String x) {
    // log.fine('on share address: ${address}');
    share.Share.share(x);
  }

  void onStartConsole() {
    startConsole = true;
  }

  List<Transfer> searchTransferMulti(final List<String> keys) =>
      transfer.searchTransferMulti(
        getAll().where((tx) => tx.amount != 0).toList(),
        keys,
      );

  List<Transfer> searchTransfer(final String key) => searchTransferMulti([key]);

  void pushHistory(final String value) {
    final _value = value.trim();
    if (_value.isEmpty) return;
    if (hasHistory()) {
      if (_value == peekHistory()) {
        return;
      }
    }
    searchCache.history.addFirst(value);
  }

  bool hasHistory() => searchCache.history.isNotEmpty;

  String peekHistory() => searchCache.history.first;

  String popHistory() => searchCache.history.removeFirst();

  bool search(final String value) {
    final _search = value.split(RegExp(r"\s+"));
    final _result = transfer.sortTxs(searchTransferMulti(_search));
    searchCache.searchResult = _result;
    return _result.isNotEmpty;
  }

  void onChangeSearch(final String value) {
    log.fine('on change search: $value');
    final _value = value.trim();
    final _found = search(_value);
    if (_found) pushHistory(_value);
    syncState();
  }

  bool shouldPop() {
    log.fine('should pop in synced, history: ${searchCache.history}');

    final _inSearchView = pageController.hasClients &&
        (pageController.page ?? 0) > (config.searchViewIndex - 0.5) &&
        (pageController.page ?? 0) < (config.searchViewIndex + 0.5);

    if (!_inSearchView) {
      return true;
    }

    final _currentKey = textEditingController.text.trim();

    if (!hasHistory()) {
      search('');
      setSearchKey('');
      return false;
    }

    final _lastKey = peekHistory();

    if (_lastKey == _currentKey) {
      popHistory();
      if (!hasHistory()) {
        search('');
        setSearchKey('');
      } else {
        final _nextLastKey = peekHistory();
        search(_nextLastKey);
        setSearchKey(_nextLastKey);
      }
    } else {
      search(_lastKey);
      setSearchKey(_lastKey);
    }

    return false;
  }

  void setSearchKey(final String key) {
    final _textSelection = TextSelection.fromPosition(
      TextPosition(offset: key.length),
    );
    textEditingController.value = textEditingController.value.copyWith(
      text: key,
      selection: _textSelection,
    );
  }

  void findByKeyword(final String _keyword) async {
    log.fine('synced state got search request: $_keyword');

    final _fullResult = searchTransfer(_keyword);

    void movePage() {
      log.fine('page: ${pageController.page}');

      if (pageController.hasClients) {
        if (pageController.page != config.searchViewIndex) {
          pageController.animateToPage(
            config.searchViewIndex,
            duration: Duration(milliseconds: 250),
            curve: Curves.ease,
          );
        }
      }
    }

    if (_fullResult.isNotEmpty) {
      await for (final _ in Stream.fromIterable(Iterable.generate(10))) {
        if (pageController.hasClients) break;
        await Future.delayed(Duration(milliseconds: 100));
      }

      movePage();
    }

    void insertSearchKey(final String key) {
      setSearchKey(key);
      pushHistory(key);
      syncState();
    }

    await for (final i
        in Stream.fromIterable(Iterable.generate(_keyword.length))) {
      // log.fine('i: ${i}');

      final _subKeyword = _keyword.substring(0, i + 1);
      log.fine('subkey: $_subKeyword');

      final _stepResult = searchTransfer(_subKeyword);
      log.fine('_stepResult: ${_stepResult.length}');

      if (_stepResult.isEmpty) {
        // log.fine('breaking on ${i}');
        if (_fullResult.isEmpty && i > 0) {
          final _text = textEditingController.text;
          final _lastGoodKey = _text.endsWith('?') ? _text : _text + '?';
          insertSearchKey(_lastGoodKey);
        }
        break;
      }

      if (_stepResult.length == _fullResult.length) {
        searchCache.searchResult = _stepResult;
        insertSearchKey(_subKeyword);
        break;
      }
    }
  }

  Future<void> showLocalNotification() async {
    if (!isActive() || txCold.txIn.isEmpty) return;

    final _newTxIn = await notification.getNewTxs(
      txCold.height,
      txCold.txIn,
    );

    SnackBar makeSnack(final Transfer x) => SnackBar(
          content: Text(
            transfer.formatNotification(x),
            // transfer.formatTx(x),
            style: TextStyle(
              color: config.c.theme.scaffoldBackgroundColor,
            ),
          ),
          action: SnackBarAction(
            label: 'View',
            onPressed: () {
              findByKeyword(x.txid);
            },
          ),
        );

    for (final x in _newTxIn) {
      // ignore: deprecated_member_use
      appHook.getScaffoldKey().currentState?.showSnackBar(makeSnack(x));
    }
  }

  List<Transfer> getIn() => txCold.txIn + txHot.txIn;
  List<Transfer> getOut() => txCold.txOut + txHot.txOut;
  List<Transfer> getPool() => txPool.getAll();
  List<Transfer> getAll() => [
        ...txCold.getAll(),
        ...txHot.getAll(),
        ...getPool(),
      ];

  Future<AppStateAutomata> next() async {
    if (await shouldExit()) {
      log.fine('saving wallet before exiting');
      await rpc.save();
      return exitState();
    }

    if (await shouldSkip()) {
      log.finest('skipping state update');
      return this;
    }

    if (appHook.rpcProcessCompleted) {
      return await reset();
    }

    if (startConsole) {
      return LoadConsoleState(
        appHook,
      );
    }

    log.finer('synced state next');

    // process intent
    if (txCold.height > 0) {
      final _hasIntent = appHook.hasIntent();
      if (_hasIntent) {
        // reload process if rpc is ded
        if (!await wallet.isRpcReady()) {
          return await reset();
        }

        final _intent = appHook.getIntent();
        // final _intent = 'wownero://resolve?method=search&keyword=163';
        final _sendData = uri.parseUri(getAll(), _intent);

        switch (_sendData.intentType) {
          case uri.IntentType.payment:
            Future<AppStateAutomata> getTransferState() async {
              await rpc.refresh();
              final _accounts = await rpc.getSubaddresssAccounts();
              final _formKey = GlobalKey<FormState>();

              final _payment = _sendData.payment!;
              final _address = _payment.address;
              final _amount = _payment.paymentInfo.hasAmount
                  ? _payment.paymentInfo.amount
                  : 0.0;
              final _paymentInfo = _payment.paymentInfo;
              return TransferState(
                appHook,
                _accounts,
                _address,
                _paymentInfo,
                _formKey,
                txCold,
                0,
                _amount,
              );
            }
            return ResyncingState(appHook, getTransferState);
          case uri.IntentType.search:
            findByKeyword(_sendData.search!.keyword);
            break;

          default:
        }
      }
    }

    if (searchNeedsUpdate && refreshed) {
      final _currentKey = textEditingController.text.trim();
      log.fine('updating search with key: $_currentKey');
      search(_currentKey);
      searchNeedsUpdate = false;
    }

    // normal state loop ends here, if already refreshed
    final _now = DateTime.now().millisecondsSinceEpoch;
    if (_now - rpcPullTimestampInMilliseconds <
        config.rpcPullIntervalInMilliseconds) {
      await waitInput();
      return this;
    }

    if (!await sensor.isRpcAndDaemonReady()) {
      return ResyncingState(appHook, () async => this);
    }

    // log.fine('accounts: ${_accounts}');

    await rpc.refresh();
    final _height = await rpc.getHeight();
    final _unlockedBalance = await rpc.getTotalUnlockedBalance();
    final _newCachedHeight = _height - config.reorgBuffer;

    // update tx cold
    if (txCold.height < _newCachedHeight) {
      final _newTxCache = await cache.updateTxCold(txCold, _newCachedHeight);

      return SyncedState(
        appHook,
        _height,
        _unlockedBalance,
        _newTxCache,
        txHot,
        txPool,
        address,
        pageController,
        textEditingController,
        searchCache,
        searchNeedsUpdate: true,
        startConsole: startConsole,
      );
    }

    // update tx hot
    await showLocalNotification();

    final _newTxHot = await cache.getTxHot(_newCachedHeight);

    final _txIn = _newTxHot.txIn + txCold.txIn;
    final _newTxPool = await cache.getTxPool(_txIn, _newCachedHeight);

    final _usedAddresses = [
      ...txCold.txIn,
      ..._newTxHot.txIn,
      ..._newTxPool.txIn
    ]
        .map((x) => x.subaddrIndex)
        .where((x) => x.major == 0)
        .map((x) => x.minor)
        .toList()
        .cast<int>();

    // log.fine('used address: ${_usedAddresses}');
    final _address = await wallet.getUnusedAddress(_usedAddresses);

    final _qrImage = (_address == address && qrImage != null)
        ? qrImage
        : qr.QrImage(
            data: _address,
            version: qr.QrVersions.auto,
            size: 200.0,
            foregroundColor: config.c.theme.scaffoldBackgroundColor,
            backgroundColor: config.c.theme.primaryColor,
            // embeddedImage: AssetImage('assets/wownero-icon-small.png'),
            embeddedImageStyle: qr.QrEmbeddedImageStyle(
              size: Size(40, 40),
            ),
          );

    final _syncedState = SyncedState(
      appHook,
      _height,
      _unlockedBalance,
      txCold,
      _newTxHot,
      _newTxPool,
      _address,
      pageController,
      textEditingController,
      searchCache,
      qrImage: _qrImage,
      rpcPullTimestampInMilliseconds: DateTime.now().millisecondsSinceEpoch,
      refreshed: true,
      searchNeedsUpdate: searchNeedsUpdate,
      startConsole: startConsole,
    );

    log.fine('full refresh: $_height');

    return _syncedState;
  }
}
