/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'package:flutter/material.dart';

import '../state.dart';
import '../helper.dart';
import '../logic/data/format.dart';

Widget build(BuildContext context, ReceiptState state) {
  final _txSucceed = state.fees.isNotEmpty;
  final double _fee =
      _txSucceed ? humanizeAmount(state.fees.fold(0, (x, y) => x + y)) : 0;

  final _goodReceipt = [
    Center(
      child: Text(
        addSymbol('🎟 ${formatAmount(_fee)}'),
        style: Theme.of(context).textTheme.headline4,
      ),
    ),
    Spacer(),
  ];

  final _badReceipt = [
    Center(
      child: Text(
        'Tx failed',
        style: Theme.of(context).textTheme.subtitle1,
      ),
    ),
    Expanded(
      child: Center(
        // ignore: deprecated_member_use
        child: OutlineButton(
          child: Text('OK'),
          onPressed: state.onOk,
          textColor: Theme.of(context).primaryColor,
          borderSide: BorderSide(color: Theme.of(context).primaryColor),
          highlightedBorderColor: Theme.of(context).primaryColor,
        ),
      ),
    ),
  ];

  return Container(
    child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Spacer(),
          ..._txSucceed ? _goodReceipt : _badReceipt,
        ]),
  );
}
