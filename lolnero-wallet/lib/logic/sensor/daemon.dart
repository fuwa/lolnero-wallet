/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'dart:async';

import 'rpc/daemon.dart' as daemonRpc;
import '../../config.dart' as config;
import '../../logging.dart';

Future<bool> isSynced() async {
  final _targetHeight = await daemonRpc.targetHeight();
  final _height = await daemonRpc.height();

  log.fine('sensor/daemon - targetHeight: $_targetHeight, height: $_height');
  return _targetHeight >= 0 &&
      _targetHeight <= _height &&
      _height > config.minimumHeight;
}

Future<bool> isConnected() async {
  final _connections = await daemonRpc.getConnectionsSimple();
  log.finer('sensor/daemon: _connections: $_connections');
  return _connections.isNotEmpty;
}

Future<bool> isReady() async => await isSynced() && await isConnected();
