import 'package:flutter/material.dart';

import 'widget/blank.dart' as blank;
import 'widget/loading.dart' as loading;
import 'widget/checkWallet.dart' as checkWallet;
import 'widget/syncing.dart' as syncing;
import 'widget/synced.dart' as synced;
import 'widget/transfer.dart' as transfer;
import 'widget/receipt.dart' as receipt;
import 'widget/waitDaemon.dart' as waitDaemon;
import 'widget/resyncing.dart' as resyncing;
import 'widget/loadConsole.dart' as loadConsole;
import 'widget/console.dart' as console;
import 'widget/exiting.dart' as exiting;
import 'state.dart';
import 'config.dart' as config;

Widget build(final BuildContext context, final AppState state) {
  Widget _getWidget() {
    switch (state.runtimeType) {
      case BlankState:
        return blank.build(context, state as BlankState);
      case LoadingState:
        return loading.build(context, state as LoadingState);
      case CheckWalletState:
        return checkWallet.build(context, state as CheckWalletState);
      case SyncingState:
        return syncing.build(context, state as SyncingState);
      case SyncedState:
        return synced.build(context, state as SyncedState);
      case TransferState:
        return transfer.build(context, state as TransferState);
      case ReceiptState:
        return receipt.build(context, state as ReceiptState);
      case WaitDaemonState:
        return waitDaemon.build(context, state as WaitDaemonState);
      case ResyncingState:
        return resyncing.build(context, state as ResyncingState);
      case LoadConsoleState:
        return loadConsole.build(context, state as LoadConsoleState);
      case ConsoleState:
        return console.build(context, state as ConsoleState);
      case ExitingState:
        return exiting.build(context, state as ExitingState);
      default:
        return Placeholder();
    }
  }

  return Scaffold(
    body: AnimatedSwitcher(
      duration: config.transitionDuration,
      child: _getWidget(),
    ),
    key: state.appHook.getScaffoldKey(),
  );
}
