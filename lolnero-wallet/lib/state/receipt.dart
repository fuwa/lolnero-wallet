/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'dart:async';

import '../logging.dart';
import '../helper.dart';
import '../logic/controller/rpc/wallet.dart' as rpc;

import 'prototype.dart';
import 'synced.dart';

class ReceiptState extends AppStateAutomata {
  final List<int> amounts;
  final List<int> fees;
  final List<String> txHashes;
  final TxCache txCache;
  bool done = false;

  ReceiptState(
    appHook,
    this.amounts,
    this.fees,
    this.txHashes,
    this.txCache,
  ) : super(appHook);

  void onOk() {
    log.fine('on Ok');
    done = true;
  }

  Future<AppStateAutomata> next() async {
    if (await shouldExit()) return exitState();

    final _txSucceed = fees.isNotEmpty;

    if (_txSucceed || done) {
      await rpc.save();
      return await SyncedState.cachedSyncedState(appHook, txCache).next();
    } else {
      await waitInput();
      return this;
    }
  }
}
