/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'dart:async';
import 'dart:core';
import 'dart:convert';
import 'dart:io';
import 'dart:collection';

import 'package:flutter/material.dart';

import '../logging.dart';
import '../helper.dart';
import '../config.dart' as config;
import '../logic/controller/process/run.dart' as run;
import '../logic/sensor/daemon.dart' as daemon;
import '../logic/sensor/wallet.dart' as wallet;

import 'prototype.dart';
import 'waitDaemon.dart';
import 'console.dart';

class LoadConsoleState extends AppStateAutomata {
  LoadConsoleState(
    appHook,
  ) : super(appHook);

  Future<AppStateAutomata> next() async {
    if (await shouldExit()) return exitState();

    if (await shouldSkip()) {
      log.finest('skipping state update');
      return this;
    }

    final _daemonReady = await daemon.isReady();
    if (!_daemonReady) {
      return WaitDaemonState(appHook, this);
    }

    final _rpc = appHook.rpcProcess;
    if (_rpc == null) {
      log.fine('console process null');
      return await reset();
    }

    log.fine('load console state next');

    // kill rpc
    _rpc.kill();
    await _rpc.exitCode.timeout(
      Duration(seconds: config.rpcKillWaitingInSeconds),
      onTimeout: () {
        log.warning('RPC exitCode timeout');
        appHook.rpcProcess?.kill(ProcessSignal.sigkill);
        return -1;
      },
    );
    await Future.delayed(
        Duration(milliseconds: config.targetInputLagInMilliseconds));

    // start cli
    final _walletPath = await wallet.walletPath();

    final _cliProcess = await run.runBinary(
      config.c.walletCliBin,
      [
        '--open',
        _walletPath,
        '--password',
        "''",
        ...config.c.extraCliArgs,
      ],
    );

    log.fine('cliProcess created');

    final _stdout =
        _cliProcess.stdout.transform(utf8.decoder).asBroadcastStream();

    final _stdoutQueue = Queue();

    AppHook _appHook = AppHook(
      appHook.setState,
      appHook.getNotification,
      appHook.isExiting,
      appHook.getInitialIntent,
      appHook.getIntent,
      appHook.hasIntent,
      appHook.getFocusNode,
      appHook.getScaffoldKey,
      _cliProcess,
      _stdoutQueue,
    );

    _stdout.listen((x) {
      log.fine(x);
      _stdoutQueue.addLast(x);
      while (_stdoutQueue.length > config.logLines) {
        _stdoutQueue.removeFirst();
      }
      _appHook.stdoutCache = _stdoutQueue.join();
    });

    _cliProcess.stderr.listen((v) {
      log.fine(v);
    });

    _cliProcess.exitCode
        .whenComplete(() => _appHook.rpcProcessCompleted = true);

    return ConsoleState(
      _appHook,
      TextEditingController(),
      _cliProcess.stdin,
    );
  }
}
