/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

import '../../state.dart';
import '../../helper.dart';
import '../../logic/data/rpc/typed/transfer.dart';
import '../../logic/data/transfer.dart' as transfer;

Widget receiptListView(final ThemeData _theme, final Iterable<String> xs) =>
    ListWheelScrollView(
      itemExtent: 100,
      perspective: 0.0012,
      offAxisFraction: 1,
      children: xs.map(receiptListItem(_theme)).toList(),
    );

typedef TxListItemFunc = Widget Function(String);

TxListItemFunc receiptListItem(final ThemeData _theme) =>
    (final String x) => Center(
          child: ListTile(
            title: Container(
              // padding: EdgeInsets.all(14),
              decoration: BoxDecoration(
                border: Border.all(
                  color: _theme.dividerColor,
                ),
              ),
              child: Align(
                alignment: Alignment.center,
                child: Text(
                  x,
                  style: _theme.textTheme.bodyText2,
                ),
              ),
            ),
          ),
        );

Widget txListView(
  final ThemeData _theme,
  final SyncedState state,
  final List<Transfer> xs,
) {
  return ListView(
    children: xs
        .map((x) => Container(
              margin: EdgeInsets.all(20),
              decoration: BoxDecoration(
                border: Border.all(
                  color: _theme.dividerColor,
                ),
              ),
              child: searchListItem(
                _theme,
                state,
                summary: true,
              )(x),
            ))
        .toList(),
  );
}

enum ListType { empty, one, many }

Widget searchListView(
  final ThemeData _theme,
  final SyncedState state,
  final List<Transfer> xs,
) {
  String showListType(List<Transfer> xs) {
    switch (xs.length) {
      case 0:
        return describeEnum(ListType.empty);
      case 1:
        return describeEnum(ListType.one);
      default:
        return describeEnum(ListType.many);
    }
  }

  if (xs.length == 1) {
    return ListView(
      children: [searchItemDetail(_theme, state, xs.first)],
      key: Key(showListType(xs)),
    );
  }

  final bool _dense = xs.length > 3;

  return ListView(
    children: xs.map(searchListItem(_theme, state, dense: _dense)).toList(),
    key: Key(showListType(xs)),
  );
}

typedef SearchListItemFunc = Widget Function(Transfer);

SearchListItemFunc searchListItem(
  final ThemeData _theme,
  final SyncedState state, {
  final bool dense = true,
  final bool clickable = true,
  final bool summary = false,
}) =>
    (final Transfer x) {
      final _title = Padding(
        padding: EdgeInsets.symmetric(vertical: dense ? 14.5 : 12),
        child: Row(children: [
          Expanded(
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                transfer.formatTitle(x),
              ),
            ),
          ),
          Flexible(
            child: Align(
                alignment: Alignment.centerRight,
                child: Text(
                  x.height == 0 ? ago(x.timestamp) : x.height.toString(),
                )),
          ),
        ]),
      );

      return Container(
        padding: EdgeInsets.all(14),
        child: ListTile(
          leading: transfer.formatIcon(x),
          dense: dense,
          isThreeLine: true,
          title: _title,
          onTap: () => state.findByKeyword(x.txid),
          subtitle: Padding(
            padding: EdgeInsets.symmetric(vertical: 5),
            child: Text(
              summary ? formatShortHash(x.txid) : transfer.formatSubtitle(x),
              style: summary
                  ? _theme.textTheme.bodyText2
                  : TextStyle(color: _theme.primaryColor),
            ),
          ),
        ),
      );
    };

Widget searchItemDetail(
  final ThemeData _theme,
  final SyncedState state,
  final Transfer x,
) {
  final _detail = Column(children: [
    transfer.formatIcon(x, size: 48),
    ...(transfer.formatDetailIcon(x, state.height)).map((x) => ListTile(
          leading: Padding(
            padding: EdgeInsets.symmetric(vertical: 0),
            child: Icon(
              x.icon,
              color: _theme.primaryColor,
              size: 36,
            ),
          ),
          title: Padding(
            padding: EdgeInsets.fromLTRB(0, 16, 0, 16),
            child: Text(
              x.summary == '' ? x.title : x.summary,
              style: TextStyle(
                color: _theme.primaryColor,
              ),
              // style: _theme.textTheme.title,
            ),
          ),
          onLongPress: x.longPressable ? () => state.onShare(x.title) : null,
          onTap: x.tappable ? () => state.findByKeyword(x.title) : null,
        )),
  ]);

  return Container(
    margin: EdgeInsets.symmetric(vertical: 6),
    padding: EdgeInsets.symmetric(horizontal: 20),
    child: _detail,
  );
}
