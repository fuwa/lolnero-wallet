/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

import 'dart:io';
import 'dart:ui';
import 'dart:collection';

import '../config.dart' as config;
import '../logic/data/rpc/typed/transfer.dart';
import '../logging.dart';
import '../helper.dart';

import 'exiting.dart';
import 'blank.dart';

typedef SetStateFunc = void Function(AppState);
typedef GetNotificationFunc = AppLifecycleState Function();
typedef IsExitingFunc = bool Function();
typedef GetInitialIntentFunc = Future<String> Function();
typedef GetIntentFunc = String Function();
typedef HasIntentFunc = bool Function();
typedef GetFocusNodeFunc = FocusNode Function();
typedef GetScaffoldKeyFunc = GlobalKey<ScaffoldState> Function();

class AppHook {
  final SetStateFunc setState;
  final GetNotificationFunc getNotification;
  final IsExitingFunc isExiting;
  final GetInitialIntentFunc getInitialIntent;
  final GetIntentFunc getIntent;
  final HasIntentFunc hasIntent;
  final GetFocusNodeFunc getFocusNode;
  final GetScaffoldKeyFunc getScaffoldKey;
  final Process? rpcProcess;
  final Queue stdout;
  String stdoutCache;
  bool rpcProcessCompleted;
  AppHook(
    this.setState,
    this.getNotification,
    this.isExiting,
    this.getInitialIntent,
    this.getIntent,
    this.hasIntent,
    this.getFocusNode,
    this.getScaffoldKey,
    this.rpcProcess,
    this.stdout, {
    this.stdoutCache = '',
    this.rpcProcessCompleted = false,
  });
}

class AppState {
  final AppHook appHook;
  int skipped = 0;
  AppState(this.appHook);

  Future<bool> shouldExit() async {
    final _isExiting = appHook.isExiting();
    if (_isExiting) return true;

    if (appHook.rpcProcess != null && appHook.rpcProcessCompleted) {
      final _exitCode = await appHook.rpcProcess?.exitCode;
      return _exitCode != 0;
    }

    return false;
  }

  ExitingState exitState() => ExitingState(appHook);

  bool isActive() {
    final _appState = appHook.getNotification();
    return _appState == AppLifecycleState.resumed;
  }

  bool shouldPop() => true;

  Future<bool> shouldSkip() async {
    if (!isActive()) {
      if (skipped < config.forcedUpdateInSeconds) {
        await tick();
        skipped++;
        return true;
      } else {
        skipped = 0;
        return false;
      }
    } else {
      skipped = 0;
      return false;
    }
  }

  void syncState() {
    appHook.setState(this);
  }

  Future<BlankState> reset() async {
    final _rpc = appHook.rpcProcess;
    if (_rpc != null) {
      _rpc.kill();
      await _rpc.exitCode.timeout(
        Duration(seconds: config.rpcKillWaitingInSeconds),
        onTimeout: () {
          log.warning('RPC exitCode timeout');
          appHook.rpcProcess?.kill(ProcessSignal.sigkill);
          return -1;
        },
      );
      await Future.delayed(
          Duration(milliseconds: config.targetInputLagInMilliseconds));
    }
    final _appHook = AppHook(
      appHook.setState,
      appHook.getNotification,
      appHook.isExiting,
      appHook.getInitialIntent,
      appHook.getIntent,
      appHook.hasIntent,
      appHook.getFocusNode,
      appHook.getScaffoldKey,
      null,
      Queue(),
    );

    return BlankState(_appHook);
  }
}

abstract class AppStateAutomata extends AppState {
  AppStateAutomata(appHook) : super(appHook);
  Future<AppStateAutomata> next();
}

class TxCache {
  final int height;
  final List<Transfer> txIn;
  final List<Transfer> txOut;
  final List<Transfer> txFailed;

  const TxCache(
    this.height,
    this.txIn,
    this.txOut,
    this.txFailed,
  );

  factory TxCache.txCacheAt(final int height) => TxCache(height, [], [], []);

  // ignore: hash_and_equals
  bool operator ==(x) =>
      x is TxCache &&
      height == x.height &&
      listEquals(txIn, x.txIn) &&
      listEquals(txOut, x.txOut) &&
      listEquals(txFailed, x.txFailed);

  List<Transfer> getAll() => txIn + txOut + txFailed;
}

const TxCache defaultTxCache = TxCache(0, const [], const [], const []);
