/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'package:flutter/services.dart' show rootBundle;

import 'dart:io';
import 'dart:async';

import '../../logging.dart';
import '../data/cert.dart';
import '../sensor/helper.dart';
import '../../config.dart' as config;
import 'process/run.dart' as process;

Future<Directory> getCertificateDir() async {
  final workingDir = await process.getWorkingDir();
  final _certificateDir = Directory(workingDir.path + '/certificate');
  await _certificateDir.create(recursive: true);
  return _certificateDir;
}

Future<String> getPriKeyPath() async {
  final _certificateDir = await getCertificateDir();
  return _certificateDir.path + "/pri.pem";
}

Future<String> getCertPath() async {
  final _certificateDir = await getCertificateDir();
  return _certificateDir.path + "/cert.pem";
}

Future<String> getCsrPath() async {
  final _certificateDir = await getCertificateDir();
  return _certificateDir.path + "/csr.pem";
}

Future<String> getOpensslCnfPath() async {
  final _certificateDir = await getCertificateDir();
  return _certificateDir.path + "/openssl.cnf";
}

Future<bool> certExist() async {
  final _certPath = await getCertPath();
  return File(_certPath).exists();
}

Future<Certificate> genCertificate() async {
  final _opensslCrf = await rootBundle.loadString('assets/openssl.cnf');
  log.fine('openssl crf: $_opensslCrf');

  final _opensslCrfPath = await getOpensslCnfPath();
  await File(_opensslCrfPath).writeAsString(_opensslCrf);

  final _priKeyPath = await getPriKeyPath();
  final _csrPath = await getCsrPath();
  final _certPath = await getCertPath();

  final sslBinaryPath = await getBinaryPath(config.c.opensslBin);
  final binaryDir = await getBinaryDir();
  final _env = {"LD_LIBRARY_PATH": binaryDir};
  log.fine('env: $_env');

  // openssl req -new -newkey rsa:4096 -nodes \
  // -keyout www.example.com.key -out www.example.com.csr \
  // -subj "/C=US/ST=Denial/L=Springfield/O=Dis/CN=www.example.com"
  final _opensslGenCert = await Process.run(
    sslBinaryPath,
    [
      'req',
      '-new',
      '-newkey',
      'rsa:4096',
      '-nodes',
      '-keyout',
      _priKeyPath,
      '-out',
      _csrPath,
      '-config',
      _opensslCrfPath,
      '-subj',
      '/C=US/ST=Denial/L=Springfield/O=Dis/CN=wallet.lolnero.org',
    ],
    environment: _env,
  );

  log.fine('gen crt output: \n${_opensslGenCert.stdout}');
  log.fine('gen crt stderr: \n${_opensslGenCert.stderr}');

  final _opensslSign = await Process.run(
    sslBinaryPath,
    [
      'x509',
      '-req',
      '-sha256',
      '-signkey',
      _priKeyPath,
      '-in',
      _csrPath,
      '-days',
      '999999',
      '-out',
      _certPath,
    ],
    environment: _env,
  );

  // log.fine('openssl output: \n${_opensslResult.stdout}');
  log.fine('sign cert stderr: \n${_opensslSign.stderr}');

  final _certCheck = await File(_certPath).readAsString();
  log.fine('cert Check: \n$_certCheck');

  return loadCertificate();
}

Future<Certificate> loadCertificate() async {
  final _priKeyPath = await getPriKeyPath();
  final _certPath = await getCertPath();

  final sslBinaryPath = await getBinaryPath(config.c.opensslBin);
  final binaryDir = await getBinaryDir();
  final _env = {"LD_LIBRARY_PATH": binaryDir};

  // openssl x509 -noout -fingerprint -sha256 -inform pem -in test-pub.pem
  final _opensslFingerprint = await Process.run(
    sslBinaryPath,
    [
      'x509',
      '-noout',
      '-fingerprint',
      '-sha256',
      '-inform',
      'pem',
      '-in',
      _certPath,
    ],
    environment: _env,
  );

  final _stdout = _opensslFingerprint.stdout;
  final _fingerprint = ('=' + _stdout).split('=').last;

  log.fine('fingerprint: \n$_fingerprint');

  return Certificate(_priKeyPath, _certPath, _fingerprint);
}

Future<Certificate> getCertificate() async {
  final _certExist = await certExist();
  if (_certExist) {
    return loadCertificate();
  } else {
    return genCertificate();
  }
}
