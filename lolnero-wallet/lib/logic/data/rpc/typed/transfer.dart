/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import '../../../../helper.dart';

import 'subaddressIndex.dart';

enum TransferType {
  txIn,
  txOut,
  poolIn,
  pendingOut,
  failed,
  block,
  none,
}

TransferType parseTransferType(final String x) {
  switch (x) {
    case 'in':
      return TransferType.txIn;
    case 'out':
      return TransferType.txOut;
    case 'pool':
      return TransferType.poolIn;
    case 'pending':
      return TransferType.pendingOut;
    case 'failed':
      return TransferType.failed;
    case 'block':
      return TransferType.block;
    default:
      return TransferType.none;
  }
}

String showTransferType(final TransferType x) {
  switch (x) {
    case TransferType.txIn:
      return 'in';
    case TransferType.txOut:
      return 'out';
    case TransferType.poolIn:
      return 'pool';
    case TransferType.pendingOut:
      return 'pending';
    case TransferType.failed:
      return 'failed';
    case TransferType.block:
      return 'block';
    default:
      return 'none';
  }
}

class Transfer {
  final int height;
  final int amount;
  final int timestamp;
  final TransferType type;
  final SubaddressIndex subaddrIndex;
  final String address;
  final int confirmations;
  final int fee;
  final String txid;

  const Transfer(
    this.height,
    this.amount,
    this.timestamp,
    this.type,
    this.subaddrIndex,
    this.address,
    this.confirmations,
    this.fee,
    this.txid,
  );

  Transfer.fromJson(final Map<String, dynamic> json)
      : height = asInt(json['height']),
        amount = asInt(json['amount']),
        timestamp = asInt(json['timestamp']),
        type = parseTransferType(asString(json['type'])),
        subaddrIndex = SubaddressIndex.fromJson(asMap(json['subaddr_index'])),
        address = asString(json['address']),
        confirmations = asInt(json['confirmations']),
        fee = asInt(json['fee']),
        txid = asString(json['txid']);

  // ignore: hash_and_equals
  bool operator ==(x) =>
      x is Transfer
      // && amount        == x.amount
      // && timestamp     == x.timestamp
      // && subaddrIndex  == x.subaddrIndex
      // && address       == x.address
      // && confirmations == x.confirmations
      // && fee           == x.fee
      // && identical(type, x.type)
      &&
      txid == x.txid;
}
