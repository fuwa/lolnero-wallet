/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'dart:async';
import 'dart:core';
import 'dart:io';

import 'package:flutter/material.dart';

import '../logging.dart';
import '../helper.dart';
import '../config.dart' as config;
import '../logic/sensor/daemon.dart' as daemon;

import 'prototype.dart';
import 'waitDaemon.dart';

class ConsoleState extends AppStateAutomata {
  final TextEditingController textController;
  final IOSink stdin;
  final int rpcPullTimestampInMilliseconds;

  ConsoleState(
    appHook,
    this.textController,
    this.stdin, {
    this.rpcPullTimestampInMilliseconds = 0,
  }) : super(appHook);

  void appendInput(final String x) {
    final _input = config.c.promptString + x;
    log.fine(_input);
    final _stdoutQueue = appHook.stdout;
    _stdoutQueue.addLast(x + '\n');
    while (_stdoutQueue.length > config.logLines) {
      _stdoutQueue.removeFirst();
    }
    appHook.stdoutCache = _stdoutQueue.join();

    stdin.writeln(x);
    stdin.flush();

    syncState();
  }

  Future<AppStateAutomata> next() async {
    if (await shouldExit()) return exitState();

    if (await shouldSkip()) {
      log.finest('skipping state update');
      return this;
    }

    if (appHook.rpcProcessCompleted) {
      return await reset();
    }

    log.fine('console state next');

    // normal state loop ends here, if already refreshed
    final _now = DateTime.now().millisecondsSinceEpoch;
    if (_now - rpcPullTimestampInMilliseconds <
        config.rpcPullIntervalInMilliseconds) {
      await waitInput();
      return this;
    }

    final _daemonReady = await daemon.isReady();
    if (!_daemonReady) {
      return WaitDaemonState(appHook, this);
    }

    return ConsoleState(appHook, textController, stdin,
      rpcPullTimestampInMilliseconds: DateTime.now().millisecondsSinceEpoch,
    );
  }
}
