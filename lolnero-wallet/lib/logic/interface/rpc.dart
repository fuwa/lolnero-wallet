/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';

import '../../config.dart' as config;
import '../../helper.dart';
import '../../logging.dart';
import '../../logic/controller/cert.dart' as cert;

HttpClient defaultClient = HttpClient();
HttpClient defaultLongWaitingClient = HttpClient();
bool defaultClientInitialized = false;

Future<void> initDefaultClient() async {
  final _priKeyPath = await cert.getPriKeyPath();
  final _certPath = await cert.getCertPath();

  final _securityContext = SecurityContext()
    ..usePrivateKey(_priKeyPath)
    ..useCertificateChain(_certPath);

  final _cert = await File(_certPath).readAsString();

  defaultClient = HttpClient(context: _securityContext)
    ..connectionTimeout = const Duration(seconds: config.rpcTimeoutInSeconds)
    ..badCertificateCallback = (cert, host, port) => cert.pem == _cert;

  defaultLongWaitingClient = HttpClient(context: _securityContext)
    ..connectionTimeout =
        const Duration(seconds: config.rpcLongWaitingTimeoutInSeconds)
    ..badCertificateCallback = (cert, host, port) => cert.pem == _cert;
}

Future<HttpClientResponse?> rpcHTTP(
  final String method,
  final bool daemon, {
  final dynamic params,
  final bool longWaiting = false,
}) async {
  final _port = daemon ? config.c.daemonPort : config.c.port;
  final _host = daemon ? config.daemonHost : config.host;

  final _scheme = daemon ? 'http' : 'https';
  final _url = '$_scheme://$_host:$_port/json_rpc';

  final String body = json.encode({
    'jsonrpc': '2.0',
    'method': method,
    ...params == null
        ? {}
        : {
            'params': params,
          }
  });
  // log.fine('body: $body');

  try {
    // final client = defaultClient;
    if (!defaultClientInitialized) {
      await initDefaultClient();
      defaultClientInitialized = true;
    }

    final client = longWaiting ? defaultLongWaitingClient : defaultClient;

    final _request = await client.postUrl(Uri.parse(_url));

    _request.headers.contentType =
        ContentType("application", "json", charset: "utf-8");
    _request.headers.chunkedTransferEncoding = false;

    _request.contentLength = utf8.encode(body).length;

    _request.write(body);

    final _response = await _request.close();

    return _response;
  } catch (e) {
    log.warning(e);
    return null;
  }
}

dynamic jsonDecode(final String responseBody) => json.decode(responseBody);

Future<dynamic> rpc(
  final String method, {
  final dynamic params,
  final String? field,
  final daemon = false,
  final bool longWaiting = false,
}) async {
  if (!daemon) {
    log.finest('rpc $method params: $params');
  }
  final response = await rpcHTTP(
    method,
    daemon,
    params: params,
    longWaiting: longWaiting,
  );

  if (response == null) return null;

  if (response.statusCode != 200) {
    return null;
  } else {
    final String _responseBody =
        await response.transform(utf8.decoder).toList().then((x) => x.join());

    // log.fine('_responseBody: ${_responseBody}');

    final _body = await compute(jsonDecode, _responseBody);
    final _result = _body['result'];

    if (!daemon) {
      log.finest('rpc $method result: $_result');
    }

    if (_result == null) return null;

    final _field = field == null ? _result : _result[field];

    return _field;
  }
}

Future<String> rpcString(
  final String method, {
  final dynamic params,
  final String? field,
  final bool daemon = false,
}) async {
  final _field = await rpc(method, params: params, field: field);
  return pretty(_field);
}
