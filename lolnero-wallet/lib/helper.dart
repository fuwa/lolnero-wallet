/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'dart:convert';
import 'dart:math';

import 'config.dart' as config;
export 'language/nullSafety.dart';

List<List> groupOf(final List xs, final int i) =>
    xs.isEmpty ? [] : [xs.take(i).toList(), ...groupOf(xs.skip(i).toList(), i)];

String trimHash(String x) => formatShortHash(x);

double humanizeAmount(int x) => x / pow(10, config.c.displayShift);
int digitizeAmount(double x) => (x * pow(10, config.c.displayShift)).floor();
String formatAmount(double x) => x.toStringAsFixed(2);

String showAmount(int x) => formatAmount(humanizeAmount(x));

String formatStealthAmount(double x) {
  if (x < 100) {
    return formatAmount(x);
  }
  final int _amount = x.floor();
  final _digits = config.c.stealthModeDigits;
  final _stealthModeBound = pow(10, _digits);
  if (x < _stealthModeBound) {
    return _amount.toString();
  } else {
    // return _amount.toString();
    final _stealthAmount = _amount % _stealthModeBound;
    return '🕶️ ' + _stealthAmount.toString().padLeft(_digits, '0');
  }
}

String showStealthAmount(int x) => formatStealthAmount(humanizeAmount(x));

List<String> groupOfString(final String x, final int i) => x.length < i
    ? [x]
    : [x.substring(0, i), ...groupOfString(x.substring(i), i)];

String formatHash(String x) {
  final xs = groupOfString(x, config.c.hashViewBlockLength);
  return xs.join('-');
}

String formatShortHash(String x) {
  final xs = groupOfString(x, config.c.hashViewBlockLength);
  return xs.take(2).join('-') + ' ...';
}

String pretty(dynamic x) {
  final JsonEncoder encoder = JsonEncoder.withIndent('');
  return encoder
      .convert(x)
      .replaceAll(RegExp(r'^{'), '\n')
      .replaceAll(RegExp(r'(["\[\],{}]|: )'), '');
}

String prettyJson(dynamic x) {
  final JsonEncoder encoder = JsonEncoder.withIndent('    ');
  return encoder.convert(x);
}

Future<void> tick() async => await Future.delayed(const Duration(seconds: 1));
Future<void> waitInput() async => await Future.delayed(
    Duration(milliseconds: config.targetInputLagInMilliseconds));

String ago(final int seconds) {
  final _receiveTime = DateTime.fromMillisecondsSinceEpoch(seconds * 1000);
  final _diff = DateTime.now().difference(_receiveTime);

  format(Duration d) => d.toString().split('.').first.padLeft(8, "0");
  final _fullDiff = format(_diff);
  if (_fullDiff.substring(0, 2) == '00') {
    return _fullDiff.substring(3, _fullDiff.length);
  } else {
    return _fullDiff;
  }
}
