/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'dart:async';

import '../logging.dart';
import '../helper.dart';
import '../logic/sensor/rpc/wallet.dart' as rpc;
import '../logic/controller/rpc/wallet.dart' as rpc;
import '../logic/sensor/rpc/daemon.dart' as daemonRpc;
import '../logic/controller/pref.dart' as pref;

import 'prototype.dart';
import 'synced.dart';

class SyncingState extends AppStateAutomata {
  final Stream<String> stdout;
  final int initialWalletHeight;
  final double walletPercent;

  SyncingState(
    appHook,
    this.stdout, {
    this.initialWalletHeight = 0,
    this.walletPercent = 0,
  }) : super(appHook);

  Future<AppStateAutomata> next() async {
    if (await shouldExit()) return exitState();

    final _walletHeight = await rpc.getHeight();
    if (initialWalletHeight == 0 && _walletHeight > 0) {
      return SyncingState(
        appHook,
        stdout,
        initialWalletHeight: _walletHeight,
      );
    }

    final _daemonHeight = await daemonRpc.height();
    log.finer('daemon height: $_daemonHeight, wallet height: $_walletHeight');

    /*
    final int _initialDistance = _daemonHeight - initialWalletHeight;
    final int _currentDistance = _daemonHeight - _walletHeight;

    final _walletPercent = _initialDistance <= 0
        ? 1
        : _currentDistance <= 0
            ? 1
            : (_initialDistance - _currentDistance) / _initialDistance;
    */

    log.finer('wallet percent: $walletPercent');

    final _walletSynced =
        (_daemonHeight == _walletHeight) && (_walletHeight != 0);

    final _walletImported = await pref.walletImported();

    if (!_walletSynced) {
      if (!_walletImported) {
        await Future.delayed(const Duration(seconds: 4));
      } else {
        await tick();
      }

      return SyncingState(
        appHook,
        stdout,
        initialWalletHeight: initialWalletHeight,
        walletPercent: 0,
      );
    }

    log.fine('wallet synced');
    await rpc.store();

    if (!_walletImported) {
      await pref.setWalletImported(true);

      // we want to reset log level
      return await reset();
    }

    return await SyncedState.cachedSyncedState(
      appHook,
      defaultTxCache,
    ).next();
  }
}
