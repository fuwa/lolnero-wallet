/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'dart:io';

import '../logging.dart';
import '../config.dart' as config;

import 'prototype.dart';
import 'blank.dart';

class ExitingState extends AppStateAutomata {
  double imageOpacityLevel = 1;
  ExitingState(appHook) : super(appHook);

  Future<void> wait() async {
    Future<void> killRpc() async {
      if (appHook.rpcProcess != null) {
        log.fine('exiting state: killing rpc process');
        appHook.rpcProcess?.kill();
        await appHook.rpcProcess?.exitCode.timeout(
          Duration(seconds: config.rpcKillWaitingInSeconds),
          onTimeout: () {
            log.warning('RPC exitCode timeout');
            appHook.rpcProcess?.kill(ProcessSignal.sigkill);
            return -1;
          },
        );
      }
      log.finer('exiting state done');
    }

    Future<void> fadeLogo() async {
      await Future.delayed(
          Duration(milliseconds: config.exitLogoDelayInMilliseconds));
      imageOpacityLevel = 0;
      syncState();
      await Future.delayed(config.transitionDurationShort);
    }

    await Future.wait([
      killRpc(),
      fadeLogo(),
    ]);
  }

  Future<AppStateAutomata> next() async {
    return BlankState(appHook);
  }
}
