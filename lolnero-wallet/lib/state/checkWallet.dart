/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'dart:async';
import 'dart:core';
import 'dart:convert';
import 'dart:collection';
import 'dart:io';

import 'package:async/async.dart' show StreamGroup;

import '../logging.dart';
import '../helper.dart';
import '../config.dart' as config;

import '../logic/data/seed.dart' as seed;
import '../logic/data/cert.dart';
import '../logic/sensor/wallet.dart' as wallet;
import '../logic/sensor/daemon.dart' as daemon;
import '../logic/controller/process/run.dart' as run;
import '../logic/controller/pref.dart' as pref;

import 'prototype.dart';
import 'syncing.dart';
import 'waitDaemon.dart';

class CheckWalletState extends AppStateAutomata {
  final bool daemonSynced;
  final Certificate cert;
  final bool seedGenerated;
  final bool walletGenerated;
  int countDown;

  CheckWalletState(
    appHook,
    this.daemonSynced,
    this.cert, {
    this.seedGenerated = false,
    this.walletGenerated = false,
    this.countDown = config.countDown,
  }) : super(appHook);

  Future<AppStateAutomata> next() async {
    if (await shouldExit()) return exitState();

    log.fine('check wallet state next');

    final _hasWallet = await wallet.hasWallet();
    final _intent = await appHook.getInitialIntent();

    if (!_hasWallet) {
      log.info('no wallet');
      pref.setWalletImported(false);

      if (_intent == '') {
        if (!seedGenerated) {
          return CheckWalletState(
            appHook,
            daemonSynced,
            cert,
            seedGenerated: true,
          );
        } else {
          if (countDown < 1) {
            return exitState();
          } else {
            await tick();
            countDown--;
            return this;
          }
        }
      }
    }

    if (_hasWallet) {
      log.fine('has wallet');
      await reset();

      final _daemonSynced = await daemon.isReady();
      if (!_daemonSynced) {
        return WaitDaemonState(appHook, this);
      }

      final _walletPath = await wallet.walletPath();

      final sslArgs = [
        '--rpc-ssl-private-key=${cert.priKeyPath}',
        '--rpc-ssl-certificate=${cert.certPath}',
        '--rpc-ssl-allowed-fingerprints=${cert.fingerprint}',
        '--rpc-ssl=enabled',
      ];

      final _walletImported = await pref.walletImported();

      final _logLevel = _walletImported ? 0 : 2;

      final _rpcReady = await wallet.isRpcReady();
      if (_rpcReady) {
        return SyncingState(
          appHook,
          Stream.empty(),
        );
      }

      final _rpcProcess = await run.runBinary(config.c.walletRpcBin, [
        '--open',
        _walletPath,
        '--password',
        "''",
        '--log-level=$_logLevel',
        ...sslArgs,
        ...config.c.extraRpcArgs,
      ]);

      log.fine('rpcProcess created');

      final _stdout = StreamGroup.merge([_rpcProcess.stdout, _rpcProcess.stderr]
              .map((x) => x.transform(utf8.decoder).transform(LineSplitter())))
          .asBroadcastStream();

      _stdout.forEach((x) => log.fine(x));

      final _stdoutQueue = Queue();

      AppHook _appHook = AppHook(
        appHook.setState,
        appHook.getNotification,
        appHook.isExiting,
        appHook.getInitialIntent,
        appHook.getIntent,
        appHook.hasIntent,
        appHook.getFocusNode,
        appHook.getScaffoldKey,
        _rpcProcess,
        _stdoutQueue,
      );

      if (_walletImported) {
        _stdout.listen((x) {
          log.fine(x);
          _stdoutQueue.addLast(x);
          while (_stdoutQueue.length > config.logLines) {
            _stdoutQueue.removeFirst();
          }
          _appHook.stdoutCache = _stdoutQueue.join('\n');
        });
      }

      _rpcProcess.exitCode
          .whenComplete(() => _appHook.rpcProcessCompleted = true);

      return SyncingState(
        _appHook,
        _stdout,
      );
    }

    if (walletGenerated) {
      final _daemonSynced = await daemon.isReady();
      if (!_daemonSynced) {
        return WaitDaemonState(appHook, this);
      }

      await waitInput();
      return this;
    }

    // intent exists
    log.fine('initial intent: $_intent');
    final _tokens = seed.tidy(_intent);

    if (!seed.isValid(_tokens)) {
      log.severe('invalid seed detected');
      return exitState();
    }

    log.fine('importing seed: $_tokens');

    final _walletPath = await wallet.walletPath();

    final _createWalletProcess = await run.runBinary(config.c.walletCliBin, [
      '--offline',
      '--password',
      "''",
      '--new',
      _walletPath,
      '--restore',
      '--electrum-seed',
      _tokens.join(' '),
    ]);

    _createWalletProcess.stdout
        .transform(utf8.decoder)
        .forEach((x) => log.fine(x));
    _createWalletProcess.stderr
        .transform(utf8.decoder)
        .forEach((x) => log.fine(x));

    // ignore: close_sinks
    final IOSink _stdinCreateWallet = _createWalletProcess.stdin;
    _stdinCreateWallet.writeln('exit');

    final _createWalletExitCode = await _createWalletProcess.exitCode;
    log.fine('wallet created, exit code: $_createWalletExitCode');

    final _rpcProcess = await run.runBinary(
        config.c.walletRpcBin,
        [
              '--open',
              _walletPath,
              '--password',
              "''",
            ] +
            config.c.extraRpcArgs);

    AppHook _appHook = AppHook(
      appHook.setState,
      appHook.getNotification,
      appHook.isExiting,
      appHook.getInitialIntent,
      appHook.getIntent,
      appHook.hasIntent,
      appHook.getFocusNode,
      appHook.getScaffoldKey,
      _rpcProcess,
      Queue(),
    );

    final _stdout = StreamGroup.merge([_rpcProcess.stdout, _rpcProcess.stderr]
            .map((x) => x.transform(utf8.decoder).transform(LineSplitter())))
        .asBroadcastStream();

    _stdout.forEach((x) => log.fine(x));

    // deal with a seed importing bug in dev-v0.8
    return CheckWalletState(
      _appHook,
      daemonSynced,
      cert,
      seedGenerated: seedGenerated,
      walletGenerated: true,
    );
  }
}
