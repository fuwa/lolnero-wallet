/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'dart:async';

import '../../logging.dart';

import 'rpc/wallet.dart' as rpc;

import '../../state/prototype.dart';
import '../data/rpc/typed/transfer.dart';

Future<TxCache> updateTxCold(final TxCache txCache, final int height) async {
  final _txs = await rpc.getTransfersFor([
    TransferType.txIn,
    TransferType.txOut,
    TransferType.failed,
  ], minHeight: txCache.height, maxHeight: height);

  final _newTxCache = TxCache(
    height,
    txCache.txIn + (_txs[TransferType.txIn] ?? []),
    txCache.txOut + (_txs[TransferType.txOut] ?? []),
    txCache.txFailed + (_txs[TransferType.failed] ?? []),
  );

  log.fine('cached transfer in: ${_newTxCache.txIn.length}');
  log.fine('cached transfer out: ${_newTxCache.txOut.length}');
  log.fine('cached transfer failed: ${_newTxCache.txFailed.length}');

  return _newTxCache;
}

Future<TxCache> getTxHot(final int height) async {
  final _txs = await rpc.getTransfersFor(
    [
      TransferType.txIn,
      TransferType.txOut,
    ],
    minHeight: height,
  );

  return TxCache(
    height,
    _txs[TransferType.txIn] ?? [],
    _txs[TransferType.txOut] ?? [],
    [],
  );
}

Future<TxCache> getTxPool(final List<Transfer> _txIn, final int height) async {
  final _txs = await rpc.getTransfersFor(
    [
      TransferType.poolIn,
      TransferType.pendingOut,
      TransferType.failed,
    ],
    minHeight: height,
  );

  final txInSet = _txIn.map((x) => x.txid).toSet();
  // log.fine('txInSet: ${txInSet.length}');

  final _poolIn = (_txs[TransferType.poolIn] ?? [])
      .where((x) => !txInSet.contains(x.txid))
      .toList();

  return TxCache(
    height,
    _poolIn,
    _txs[TransferType.pendingOut] ?? [],
    _txs[TransferType.failed] ?? [],
  );
}
